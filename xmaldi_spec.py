#!/usr/bin/env python
# -*- coding: latin-1 -*-
#
"""
spec 105 (xmaldi 201)
20 April 2007
"""
#
import wx
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
#
#
class PcaWinResult(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1)
        let = (70, 15)
        num = (60, 15)
        self.SetBackgroundColour("yellow")
        self.sizer = wx.GridSizer(rows=5, cols=2, hgap=2, vgap=2)
        self.instances = {}
        #
        self.titles = [('Sensitivity', 'sensitivity'),
                       ('Specificity', 'specificity'),
                       ('Predictive Value', 'positivePredictiveValue'),
                       ('% Corrects', 'percentageCorrectlyClassified')
                       ]
        #
        self.create_cells(num, let)
        self.SetSizer(self.sizer)
        self.Fit()

    # noinspection PyArgumentList
    def create_cells(self, num, let):
        self.sizer.Add(wx.Panel(self, size=let), 1)
        self.sizer.Add(wx.Panel(self, size=let), 1)
        for eachLabel in self.titles:
            self.sizer.Add(wx.StaticText(self, label=eachLabel[0], size=let,
                           style=wx.ALIGN_CENTER))
            instance = wx.TextCtrl(self, size=num)
            self.sizer.Add(instance, 0)
            self.instances[eachLabel[0]] = instance
#
#
class PcaWinImg(wx.Panel):
    def __init__(self, parent, id_=-1, pos=wx.DefaultPosition, size=(350, 650)):
        wx.Panel.__init__(self, parent, id_, pos, size, wx.RAISED_BORDER)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.filename = ''
        self.img = None
        #
        self.image_panel = wx.Panel(self)
        # noinspection PyArgumentList
        sizer.Add(self.image_panel, 1, flag=wx.EXPAND)
        self.image_panel.bmp = None
        #
        self.SetSizer(sizer)
        self.Fit()

    def load_image(self, img=None):
        self.img = wx.Image(img, wx.BITMAP_TYPE_ANY)
        self.show_image(self.img)

    def on_size_window(self, evt):
        if self.img:
            self.show_image(self.img)
        evt.Skip()

    # noinspection PyUnresolvedReferences
    def show_image(self, image):
        xw, yw = self.GetSize()
        self.image_panel.ClearBackground()
        if self.image_panel.bmp:
            self.image_panel.bmp.Destroy()
        image = image.Scale(xw, yw)
        self.image_panel.bmp = wx.StaticBitmap(self.image_panel, -1,
                                               wx.BitmapFromImage(image))
        self.image_panel.Refresh()
#
#
class ControlCut(wx.Panel):
    # noinspection PyUnresolvedReferences
    def __init__(self, parent, position):
        wx.Panel.__init__(self, parent, pos=position, size=(200, 40))
        psx, psy = 5, 3
        szy = 21
        self.data_ini = wx.TextCtrl(self, -1,
                                    pos=(psx, psy), size=(48, szy),
                                    style=wx.TE_READONLY)
        self.bt_ini = wx.Button(self, -1, 'ini',
                                pos=(psx + 48, psy), size=(25, szy))
        self.data_fin = wx.TextCtrl(self, -1,
                                    pos=(psx + 74, psy), size=(48, szy),
                                    style=wx.TE_READONLY)
        self.bt_find = wx.Button(self, -1, 'end',
                                 pos=(psx + 123, psy), size=(25, szy))
        self.reset = wx.Button(self, -1, 'reset',
                               pos=(psx + 155, psy), size=(38, szy))
        self.SetBackgroundColour("light gray")
#
#
class CanvasMain(wx.Panel):
    # noinspection PyArgumentList
    def __init__(self, parent, xlabel='x_lab', ylabel='y_lab', choices=None):
        if not choices:
            choices = []
        wx.Panel.__init__(self, parent)
        #
        self.parent = parent
        self.clicked_mass = 0
        self.choice = choices
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.SetBackgroundColour("white")
        #
        self.figure = Figure(figsize=(3.5, 2))   # (x,y) in inches!!
        self.canvas = FigureCanvas(self, -1, self.figure)
        #
        self.add_toolbar()
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        sizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        self.SetSizer(sizer)
        self.Fit()
        #
        self.xlim = (0, 1)
        self.ylim = (0, 1)
        self.do_figure()

    def add_toolbar(self):
        self.toolbar = NavigationToolbar2Wx(self.canvas)
        #
        self.toolbar.SetToolBitmapSize(wx.Size(21, 25))
        self.toolbar.SetMinSize((1100, 31))
        #
        self.toolbar_color = self.toolbar.GetBackgroundColour()
        #
        self.bt_clean = wx.Button(self.toolbar, -1, 'Clean',
                                  pos=(190, 0), size=(35, 30))
        self.Bind(wx.EVT_BUTTON, self.on_clean, self.bt_clean)
        #
        self.toolbar.DeleteToolByPos(6)
        self.toolbar.DeleteToolByPos(5)
        self.toolbar.Realize()

    # noinspection PyUnusedLocal
    def on_clean(self, evt):
        self.clean()

    def clean(self):
        """
        """
        self.axes.figure.clear()
        self.do_figure()

    def do_figure(self):
        """Creates axes and draws canvas"""
        self.axes = self.figure.add_subplot(111)
        self.dibuja()

    # noinspection PyTypeChecker
    def dibuja(self, clean=True):
        """draw canvas
        """
        self.axes.set_xlabel(self.xlabel, size='small')
        self.axes.set_ylabel(self.ylabel, size='small')
        if not clean:
            self.axes.set_xlim(self.xlim)
            self.axes.set_ylim(self.ylim)
        self.set_tick_size()
        self.canvas.draw()

    def set_tick_size(self):
        tlx = list(self.axes.get_xticklabels())
        tlx.extend(self.axes.get_yticklabels())
        for label in tlx:
            label.set_fontsize('xx-small')
#
#
class GraphWin(CanvasMain):
    """Canvas to draw PCA distributions.
    """
    def __init__(self, parent, xlabel='x_lab', ylabel='y_lab', choices=None):
        if not choices:
            choices = []
        CanvasMain.__init__(self, parent, xlabel, ylabel, choices)

    def add_toolbar(self):
        CanvasMain.add_toolbar(self)
        self.bt_clean.SetPosition((140, 0))
        self.toolbar.DeleteToolByPos(2)
        self.toolbar.DeleteToolByPos(1)
        self.toolbar.InsertSeparator(1)
        #
        self.toolbar.SetMinSize((1100, 31))
        self.toolbar.Realize()
        psx = 195
        xlabel = wx.StaticText(self.toolbar, -1,
                               'X', pos=(psx, 9), size=(9, 13))
        ylabel = wx.StaticText(self.toolbar, -1,
                               'Y', pos=(psx + 62, 9), size=(9, 13))
        xlabel.SetBackgroundColour(self.toolbar_color)
        ylabel.SetBackgroundColour(self.toolbar_color)
        self.choice_x_box = wx.Choice(self.toolbar, -1, pos=(psx + 10, 5),
                                      size=(50, 25), choices=self.choice)
        self.choice_y_box = wx.Choice(self.toolbar, -1, pos=(psx + 73, 5),
                                      size=(50, 25), choices=self.choice)
        self.choice_x_box.SetSelection(0)
        self.choice_y_box.SetSelection(1)
        self._3d = wx.Button(self.toolbar, -1, '3D',
                             pos=(340, 0), size=(40, 30))
#
#
class SpecPcaDfa(CanvasMain):
    def __init__(self, parent, xlabel='x_lab', ylabel='y_lab', choices=None):
        if not choices:
            choices = []
        CanvasMain.__init__(self, parent, xlabel, ylabel, choices)

    def add_toolbar(self):
        CanvasMain.add_toolbar(self)
        mass_txt = wx.StaticText(self.toolbar, label='scan',
                                 pos=(235, 7), size=(25, 17))
        mass_txt.SetBackgroundColour(self.toolbar_color)
        self.mass = wx.TextCtrl(self.toolbar,
                                pos=(263, 4), size=(55, 22),
                                style=wx.TE_READONLY)
        self.deselect = wx.Button(self.toolbar, -1, 'Deselect',
                                  pos=(330, 5), size=(50, 22))
        self.deselect.SetName(self.parent.label)
        self.loads = wx.Button(self.toolbar, -1, 'Loads',
                               pos=(395, 5), size=(50, 22))
        self.loads_select = wx.SpinCtrl(self.toolbar,
                                        pos=(447, 5), size=(38, 22),
                                        min=1, max=4, initial=1)
        self.Bind(wx.EVT_BUTTON, self.on_deselect, self.deselect)
        self.Bind(wx.EVT_BUTTON, self.on_loads, self.loads)
        self.canvas.mpl_connect('motion_notify_event', self.on_motion)

    def on_motion(self, evt):
        if evt.inaxes:
            offset, dummy = self.get_offset()
            xpos = int(round(evt.xdata + offset))
            self.mass.SetValue(' %i' % xpos)

    def get_offset(self):
        return [0, 0]

    def on_deselect(self, evt):
        evt.Skip()

    def on_loads(self, evt):
        evt.Skip()
#
#
# noinspection PyUnusedLocal
class SpecMaldi(CanvasMain):
    def __init__(self, parent, xlabel='x_lab', ylabel='y_lab', choices=None):
        if not choices:
            choices = []
        CanvasMain.__init__(self, parent, xlabel, ylabel, choices)
        self.of_ini = 0
        self.of_end = 0

    def add_toolbar(self):
        CanvasMain.add_toolbar(self)
        psx = 350
        self.mass_txt = wx.StaticText(self.toolbar, label='scan',
                                      pos=(235, 7), size=(25, 17))
        self.mass_txt.SetBackgroundColour(self.toolbar_color)
        self.mass = wx.TextCtrl(self.toolbar, pos=(263, 4), size=(55, 22),
                                style=wx.TE_READONLY)
        self.control = ControlCut(self.toolbar, position=(psx, 2))
        self.control.SetBackgroundColour(self.toolbar_color)
        self.cbx_norm = wx.CheckBox(self.toolbar, label='normalize',
                                    pos=(psx + 210, 5), size=(80, 20))
        self.cbx_norm.SetBackgroundColour(self.toolbar_color)
        self.Bind(wx.EVT_BUTTON, self.on_ini, self.control.bt_ini)
        self.Bind(wx.EVT_BUTTON, self.on_fin, self.control.bt_find)
        self.Bind(wx.EVT_BUTTON, self.on_reset, self.control.reset)
        self.Bind(wx.EVT_CHECKBOX, self.on_norm, self.cbx_norm)
        self.canvas.mpl_connect('button_press_event', self.on_click_mass)
        self.canvas.mpl_connect('motion_notify_event', self.on_motion)
        #
        self.toolbar.update()

    def on_motion(self, evt):
        """Set x-position of the mouse in mass window"""
        if evt.inaxes:
            offset, dummy = self.get_offset()
            xpos = int(round(evt.xdata + offset))
            self.mass.SetValue(' %i' % xpos)

    def get_offset(self):
        """To be implemented on child."""
        return 0, 0

    def get_offset_original(self):
        """To be implemented on child."""
        return 0, 0

    def set_offset(self, a, b):
        """To be implemented on child."""
        pass

    def on_ini(self, evt):
        self.of_ini, self.of_end = self.get_offset()
        self.control.data_ini.Clear()
        self.of_ini += self.clicked_mass
        self.control.data_ini.WriteText(str(self.of_ini))
        self.set_offset(self.of_ini, None)

    def on_fin(self, evt):
        self.of_ini, self.of_end = self.get_offset()
        self.control.data_fin.Clear()
        self.of_end = self.clicked_mass + self.of_ini
        self.control.data_fin.WriteText(str(self.of_end))
        self.set_offset(None, self.of_end)

    def on_reset(self, evt):
        self.control.data_ini.Clear()
        self.control.data_fin.Clear()
        self.of_ini, self.of_end = self.get_offset_original()
        self.set_offset(self.of_ini, self.of_end)
        self.control.data_ini.WriteText(str(self.of_ini))
        self.control.data_fin.WriteText(str(self.of_end))

    def on_norm(self, evt):
        """To be implemented on child"""
        pass

    def on_click_mass(self, evt):
        xpos = evt.xdata
        try:
            xpos = int(round(xpos))
        except TypeError:
            return
        self.clicked_mass = xpos


if __name__ == '__main__':

    app = wx.App()
    panels = [PcaWinResult, PcaWinImg, SpecPcaDfa, GraphWin, SpecMaldi]
    for panel in panels:
        fr = wx.Frame(None)
        fr.label = 'test'
        panel(fr)
        fr.Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
