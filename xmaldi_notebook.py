#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
notebook 112 (xmaldi 201)
10 october 2009
"""
#
TITLE = ' Kim Maldi vs.2.3.0'
#
import wx
import wx.html
import os
import scipy
import _thread as thread
import logging
from commons.iconic import Iconic
from commons.warns import tell
from xmaldi_images import ICON
from xmaldi_common import Common, TEXT_PYCHEM, LamaBmp, collection
from xmaldi_panels import MaldiPanel, HeatPanel, PcaPanel, DfaPanel
from xmaldi_funcs import load_file, get_line_range, build_matrix
from xmaldi_funcs import norm01, save_table
#
#
log0 = logging.getLogger(__name__)
#
#
# noinspection PyUnusedLocal
class Nbook(wx.Notebook):
    """Xmaldi notebook"""
    # noinspection PyUnresolvedReferences
    def __init__(self, parent, id_, groups, data=None, files=None):
        """Nbook constructor"""
        files = files or []
        wx.Notebook.__init__(self, parent, id_, size=(210, 210),
                             style=wx.BK_DEFAULT
                             )
        # self.parent = parent
        self.data = data or {}    # data = line, column, init_dir...
        self.groups = groups      # groups = ['Control', 'Case', 'C']
        self.klasses = []         # klasses = ['train1','train4','test1', ...]
        self.collection = None    # collection = {klass:[list_file_paths],...}
        self.deselected = []
        self.lock = thread.allocate_lock()
        #
        self.win1 = MaldiPanel(self, groups, data, files)
        self.AddPage(self.win1, 'Mass Spectra')
        #
        self.win2 = HeatPanel(self, groups)
        self.AddPage(self.win2, 'Heat View')
        #
        self.win4 = PcaPanel(self, groups, data)
        self.AddPage(self.win4, 'PyChem PCA')
        #
        self.win5 = DfaPanel(self, groups, data)
        self.AddPage(self.win5, 'PyChem DFA')
        #
        self.win6 = wx.Panel(self)
        #
        self.win7 = wx.html.HtmlWindow(self)
        #
        doc = "doc/README.html"
        if os.path.exists(doc):
            self.win7.LoadPage(doc)
        else:
            self.win7.LoadFile('README.txt')
        #
        # noinspection PyProtectedMember
        try:
            bt_tables = wx.BitmapButton(self.win6, -1,
                                        LamaBmp().bmp, pos=(190, 50))
        except wx._core.PyAssertionError:
            bt_tables = wx.Button(self.win6, label='RUN',
                                  pos=(190, 50), size=(80, 50))
        #
        wx.StaticText(self.win6, -1, label=TEXT_PYCHEM, pos=(100, 150))
        self.AddPage(self.win6, "PyChem_Tables")
        self.AddPage(self.win7, "Help")
        #
        self.Bind(wx.EVT_BUTTON, self.on_run_PCA, self.win4.bt_run)
        self.Bind(wx.EVT_BUTTON, self.on_run_DFA, self.win5.bt_run)
        self.Bind(wx.EVT_BUTTON, self.on_new_color_range, self.win2.bt_range)
        self.Bind(wx.EVT_BUTTON, self.on_build_table, bt_tables)
        self.Bind(wx.EVT_BUTTON, self.on_deselect, self.win4.pan_bi.deselect)
        self.Bind(wx.EVT_BUTTON, self.on_deselect, self.win5.pan_bi.deselect)
        self.win1.Bind(wx.EVT_BUTTON, self.on_signal)
    #
    def on_run_PCA(self, evt):
        """Activated from PcaPanel when pressing the button"""
        parent = self.win4
        program = 'calcScipy'
        build = 1
        self.build_and_run(parent, program, build)
    #
    def on_run_DFA(self, evt):
        """Activated from PcaPanel when pressing the button"""
        parent = self.win5
        program = 'calcScipy'
        build = 2
        self.build_and_run(parent, program, build)
    #
    # noinspection PyUnresolvedReferences
    def on_deselect(self, evt):
        """Deselects file from Maldi Spectra ChecklistBox

        The spectrum shown in the Dfa or Pca panel can be eliminated from the
        main panel selection checklist if desired. In this way spectra
        can be filtered (i.e for a second analysis of the set)

        """
        evt_obj = evt.GetEventObject()
        # object Names must be set for this to work
        if evt_obj.Name == self.win4.label:
            selected = self.win4.selected
        elif evt_obj.Name == self.win5.label:
            selected = self.win4.selected
        #
        self.win4.selected = None
        self.win5.selected = None
        #
        # groups = ['Control', 'Case', 'C']
        for index, group in enumerate(self.groups):
            group_win = getattr(self.win1, self.groups[index])
            current_dir = group_win.current_dir
            checklist = group_win.checklist
            n = checklist.GetCount()
            for i in range(n):
                if checklist.IsChecked(i):
                    dfile = checklist.GetString(i)
                    dfile_path = os.path.join(current_dir, dfile)
                    # noinspection PyUnboundLocalVariable
                    if dfile_path == selected:
                        checklist.Check(i, False)
    #
    # noinspection PyUnresolvedReferences
    def build_and_run(self, parent, program, build):
        """Collects variables and executes program.

        Reads the name of the selected classes for each group,
        appends the corresponding files (collection) and executes the program.

        groups = ['Control', 'Case', 'C']
        klasses = ['train1', 'train4', 'test1', 'test4']
        collection = {klass1=[list of files],...}
        """
        self.klasses = []
        self.collection = {}
        groups_used = 0
        for index, group in enumerate(self.groups):
            klass = parent.pan_td.instances[group].GetStringSelection()
            self.klasses.append(klass)
            if klass in ['', 'None']:
                continue
            group_win = getattr(self.win1, self.groups[index])
            current_dir = group_win.current_dir
            checklist = group_win.checklist
            n = checklist.GetCount()
            dfile_paths = []
            files_used = 0
            for i in range(n):
                if checklist.IsChecked(i):
                    dfile = checklist.GetString(i)
                    dfile_path = os.path.join(current_dir, dfile)
                    dfile_paths.append(dfile_path)
                    files_used += 1
            if files_used > 1:
                groups_used += 1
            self.collection[klass] = dfile_paths

        if groups_used < build:
            tell('%s needs at least %s groups' % (program, build))
            return
        #
        try:
            # noinspection PyCallingNonCallable
            getattr(parent, program)(self.collection, self.klasses)
        except IndexError as data:
            log0.error('brn, Error %s' % data)
    #
    def on_new_color_range(self, evt):
        """ """
        self.win2.bt_range.Enable(False)
        yranges = self.win2.yranges
        index = 0
        for yrange in yranges:
            if not yrange == (None, None):
                group = (self.groups[index], None)
                index += 1
                self.on_mix(group)
        thread.start_new_thread(self.connect_button, (1,))
    #
    def connect_button(self, dummy):
        self.lock.acquire_lock()
        self.win2.bt_range.Enable(True)
        self.lock.release_lock()
    #
    # noinspection PyUnresolvedReferences
    def on_build_table(self, evt):
        """"""
        for group in self.groups:            # groups = ['Control', 'Case', 'C']
            group_win = getattr(self.win1, group)
            current_dir = group_win.current_dir
            checklist = group_win.checklist
            n = checklist.GetCount()
            dfile_paths = []
            for i in range(n):
                if checklist.IsChecked(i):
                    dfile = checklist.GetString(i)
                    dfile_path = os.path.join(current_dir, dfile)
                    dfile_paths.append(dfile_path)
            collection[group] = dfile_paths

        matrix, num, num_scans = build_matrix(collection, self.data, Common)
        save_table(matrix, collection, Common.data_maldi_dir)
    #
    def on_signal(self, evt):
        """Retrieve events from objects on child panels and reroute them
        to the corresponding call in this frame.

        In case the function is not implemented here the event is Skip() to the
        child frames

        """
        co = evt.GetEventObject()
        # wid[0] = '|'/'control'/'case'/...
        # wid[1] = on_mix/onMerge/...etc
        wid = co.GetName().split('___')
        if wid[0] == '|':
            try:
                func = getattr(self, wid[1])
                func(None)
            except AttributeError:
                evt.Skip()
        else:
            try:
                func = getattr(self, wid[1])
            except AttributeError:
                evt.Skip()
            except IndexError:
                return
            else:
                # noinspection PyCallingNonCallable
                func(wid)
    #
    def on_a_mix(self, evt):
        for group in self.win1.groups:
            win = getattr(self.win1, group)
            checked = win.checklist.GetCount()
            if not checked:
                continue
            self.on_mix((group,))
    #
    # noinspection PyUnresolvedReferences
    def on_mix(self, wid):
        """Threaded not to collapse GUI"""
        parent = getattr(self.win1, wid[0])         # self.win1.A
        if parent.files:
            parent.instances['Mix'].Enable(False)
            thread.start_new_thread(self.threaded_on_mix, (wid,))
    #
    # noinspection PyUnresolvedReferences
    def threaded_on_mix(self, wid):
        """Plot spectra in a colored matplotlib image.
        """
        self.lock.acquire_lock()
        spectra = []
        parent = getattr(self.win1, wid[0])         # self.win1.A
        #
        line_range = get_line_range(parent.files[0], Common)
        for dfile_path in parent.files:
            # don't normalize
            spectrum = load_file(dfile_path, line_range, self.data)
            if spectrum:
                spectra.append(spectrum)
        #
        tab = scipy.array(spectra)
        #
        tab_norm = norm01(tab) if Common.normalize else tab
        #
        num_plots = len(self.win1.groups)
        index = self.win1.groups.index(wid[0]) + 1
        #
        self.win2.add_plot(tab_norm, num_plots, index)
        parent.instances['Mix'].Enable(True)
        self.lock.release_lock()
#
#
class Maldi(wx.Frame, Iconic):
    """Main Window containing the Notebook. Created by InitWin.
    """
    def __init__(self, groups=None, line_range=(), data=None, init_dir='',
                 files=None):
        """"""
        files = files or []
        data = data or {}
        groups = groups or []
        #
        wx.Frame.__init__(self, None, title=TITLE, size=(655, 550))
        Iconic.__init__(self, icon=ICON)
        #
        Common.offset = list(line_range)      # eliminated part, reading range
        Common.offset_temp = list(line_range)
        Common.original = line_range          # Holds the value read in InitWin.
        Common.init_dir = init_dir            # initial directory
        Common.range_changed = False
        Common.normalize = False
        #
        self.notebook = Nbook(self, -1, groups, data, files)
        self.Bind(wx.EVT_CLOSE, self.on_close_window)
    #
    # noinspection PyUnusedLocal
    def on_close_window(self, evt):
        self.Destroy()


if __name__ == '__main__':
    mydata = {'separator': '\t', 'column_y': 1, 'column_x': None}
    path = os.path.join(os.getcwd(), "test/1/1B.txt")
    app = wx.App()
    maldi = Maldi(groups=['A', 'B'],
                  line_range=(1, 48), init_dir=path, data=mydata)
    maldi.Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
