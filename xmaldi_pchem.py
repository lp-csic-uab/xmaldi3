#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""
pchem 105 (xmaldi 201)
10 october 2009

"""
#
import copy
import logging
import scipy
import scipy.linalg
from scipy import sqrt, reshape, transpose
#
#
log7 = logging.getLogger(__name__)
#
#
def __flip__(a, axis=0):
    """Reverse order of array elements along axis 0 or 1
    """
    if axis == 0:
        return scipy.flipud(a)
    elif axis == 1:
        return scipy.fliplr(a)
#
#
def _index(y, num):
    """Used to get tuple index for take"""
    idx = [i for i in range(y.shape[0]) if y[i] == num]
    return tuple(idx)
#
#
def _put(a, ind, v):
    """a pvt put function"""
    for c, each in enumerate(ind):
        a[each, :] = v[c, :]
    return a
#
#
def __split__(xdata, ydata, mask, labels=None):
    """Splits x and y inputs into training, cross validation (and
    independent test groups) for use with modelling algorithms.

    """
    x1 = scipy.take(xdata, _index(mask, 0), 0)
    x2 = scipy.take(xdata, _index(mask, 1), 0)
    y1 = scipy.take(ydata, _index(mask, 0), 0)
    y2 = scipy.take(ydata, _index(mask, 1), 0)
    n1, n2 = [], []
    if labels is not None:
        for i in range(len(labels)):
            if mask[i] == 0:
                n1.append(labels[i])
            elif mask[i] == 1:
                n2.append(labels[i])

    if max(mask) == 1:
        return x1, x2, y1, y2, n1, n2
    elif max(mask) == 2:
        x3 = scipy.take(xdata, _index(mask, 2), 0)
        y3 = scipy.take(ydata, _index(mask, 2), 0)
        n3 = []
        if labels is not None:
            for i in range(len(labels)):
                if mask[i] == 2:
                    n3.append(labels[i])
        return x1, x2, x3, y1, y2, y3, n1, n2, n3
#
#
# noinspection PyPep8Naming
def __BW__(X, group2):
    """Generate B and W matrices for CVA.

    Original pychem function would accept only groups like [1,1,2,2,2,3,3]
    Was modified to accept groups like [2,2,4,4]

    Ref. Krzanowski

    """
    indice = 1
    group = []
    preitem = Bo = Wo = None
    start = True
    for item in group2:
        if start:
            group.append(indice)
            preitem = item
            start = False
            continue

        if item != preitem:
            indice += 1
            preitem = item
        group.append(indice)

    mx = scipy.mean(X, 0)[scipy.newaxis, :]
    for x in range(1, int(max(group))+1):
        idx = _index(scipy.array(group, 'i')[:, scipy.newaxis], int(x))
        L = len(idx)
        meani = scipy.mean(scipy.take(X, idx, 0), 0)[scipy.newaxis, :]
        meani = scipy.resize(meani, (len(idx), X.shape[1]))
        A = scipy.mean(scipy.take(X, idx, 0), 0) - mx
        C = scipy.take(X, idx, 0) - meani
        if x == 1:
            Bo = L * scipy.dot(transpose(A), A)
            Wo = scipy.dot(transpose(C), C)
        else:
            Bo = Bo + L * scipy.dot(transpose(A), A)
            Wo = Wo + scipy.dot(transpose(C), C)

    B = (1.0/(max(group)-1)) * Bo
    W = (1.0/(X.shape[0] - max(group))) * Wo

    return B, W
#
#
def meancent(myarray):
    """Mean-centre array (in-place) along axis 0

    Formerly SP_meancent

    >> a = scipy.array([[1,2,3,4],[0.1,0.2,-0.7,0.6]])
    >> a
    array([[ 1. ,  2. ,  3. ,  4. ],
           [ 0.1,  0.2, -0.7,  0.6]])
    >> scipy.mean(a)
    array([ 2.5 ,  0.05])
    >> SP_meancent(a)
    >> a
    array([[ 0.45,  0.9 ,  1.85,  1.7 ],
           [-0.45, -0.9 , -1.85, -1.7 ]])
    """
    means = scipy.mean(myarray, axis=0)   # Get the mean of each colm
    return scipy.subtract(myarray, means)
#
#
def autoscale(a):
    """Auto-scale array

    >> a = array([[1,2,3,4],[0.1,0.2,-0.7,0.6],[5,1,7,9]])
    >> a
    array([[ 1. ,  2. ,  3. ,  4. ],
           [ 0.1,  0.2, -0.7,  0.6],
           [ 5. ,  1. ,  7. ,  9. ]])
    >> a = autoscale(a)
    >> a
    array([[-0.39616816,  1.03490978, -0.02596746, -0.12622317],
           [-0.74121784, -0.96098765, -0.98676337, -0.93089585],
           [ 1.137386  , -0.07392213,  1.01273083,  1.05711902]])
    """
    mean_cols = scipy.resize(sum(a, 0) / a.shape[0], a.shape)
    std_cols = scipy.resize(scipy.sqrt(
        (sum((a - mean_cols)**2, 0))/(a.shape[0]-1)), a.shape)
    return (a-mean_cols)/std_cols
#
#
def PCA_NIPALS(myarray, comps, kind='covar'):
    """Run principal components analysis (PCA) using NIPALS

    Non-linear iterative partial least squares (NIPALS) is an algorithm for
    omputing the first few components in a principal component or partial least
    squares analysis. For very high-dimensional datasets, such as those
    generated in the 'omics sciences it is usually only necessary to compute the
    first few principal components. The  NIPALS algorithm calculates t1 and p1'
    from X. The outer product, t1p1' can then be subtracted from X leaving the
    residual matrix E1. This can be then used to calculate subsequent PCs.
    This results in a dramatic reduction in computational time since calculation
    of the covariance matrix is avoided (wiki).

    Martens,H; Naes,T: Multivariate Calibration, Wiley: New York, 1989
    """

    t1 = p1 = None

    X = copy.deepcopy(myarray)
    if kind == 'covar':
        myarray = meancent(myarray)
    elif kind == 'corr':
        myarray = autoscale(myarray)

    arr_size = myarray.shape
    tt = scipy.zeros((arr_size[0], comps), 'd')
    pp = scipy.zeros((comps, arr_size[1]), 'd')
    i = 0

    while i < comps:
        std = scipy.std(myarray, axis=0)
        st2 = scipy.argsort(std)
        ind = st2[arr_size[1]-1, ]
        t0 = myarray[:, ind]
        c = 0
        while c == 0:  # NIPALS
            p0 = scipy.dot(transpose(t0), myarray)
            p1 = p0 / sqrt(scipy.sum(p0**2))
            t1 = scipy.dot(myarray, transpose(p1))
            if sqrt(scipy.sum(t1**2)) - sqrt(scipy.sum(t0**2)) < 5*10**-5:
                tt[:, i] = t1
                pp[i, :] = p1
                c = 1
            t0 = t1

        myarray = myarray - scipy.dot(scipy.resize(t1, (arr_size[0], 1)),
                                      scipy.resize(p1, (1, arr_size[1])))

        i += 1

    # work out percentage explained variance
    X = meancent(X)
    s0, s = scipy.sum(scipy.sum(X**2)), []
    for n in scipy.arange(1, comps+1, 1):
        E = X - scipy.dot(tt[:, 0:n], pp[0:n, :])
        s.append(scipy.sum(scipy.sum(E**2)))

    pr = (1-(scipy.asarray(s)/s0)) * 100
    pr = reshape(pr, (1, len(pr)))
    pr = scipy.concatenate((scipy.array([[0.0]]), pr), 1)
    pr = reshape(pr, (pr.shape[1],))
    eigs = scipy.array(s)

    return tt, pp, pr[:, scipy.newaxis], eigs[:, scipy.newaxis]
#
#
# noinspection PyPep8Naming
def DFA(X, group, nofac, pcloads=None):
    """Discriminant function analysis

    Ref. Krzanowski

    Manly, B.F.J. Multivariate Statistical Methods: A Primer,
    2nd Ed, Chapman & Hall: New York, 1986

    > X = scipy.array([[ 0.19343116,  0.49655245,  0.72711322,  0.79482108,  0.13651874],
                       [ 0.68222322,  0.89976918,  0.30929016,  0.95684345,  0.01175669],
                       [ 0.3027644 ,  0.82162916,  0.83849604,  0.52259035,  0.89389797],
                       [ 0.54167385,  0.64491038,  0.56807246,  0.88014221,  0.19913807],
                       [ 0.15087298,  0.81797434,  0.37041356,  0.17295614,  0.29872301],
                       [ 0.69789848,  0.66022756,  0.70273991,  0.9797469 ,  0.66144258],
                       [ 0.378373  ,  0.34197062,  0.54657115,  0.27144726,  0.28440859],
                       [ 0.8600116 ,  0.2897259 ,  0.4448802 ,  0.25232935,  0.46922429],
                       [ 0.85365513,  0.34119357,  0.69456724,  0.8757419 ,  0.06478112],
                       [ 0.59356291,  0.53407902,  0.62131013,  0.73730599,  0.98833494]])
    > group = scipy.array([[1],[1],[1],[1],[2],[2],[2],[3],[3],[3]])
    > B,W = __BW__(X,group)
    > B
    array([[ 0.12756749, -0.10061061,  0.00366132, -0.00615551,  0.05378535],
           [-0.10061061,  0.09289765,  0.00469185,  0.03883801, -0.05465494],
           [ 0.00366132,  0.00469185,  0.0043456 ,  0.01883603, -0.00530158],
           [-0.00615551,  0.03883801,  0.01883603,  0.08554211, -0.0332867 ],
           [ 0.05378535, -0.05465494, -0.00530158, -0.0332867 ,  0.03372716]])
    > W
    array([[ 0.049357  ,  0.00105553, -0.00808075,  0.04037998, -0.02013773],
           [ 0.00105553,  0.03555862, -0.00982256,  0.00761902,  0.02439148],
           [-0.00808075, -0.00982256,  0.03519157,  0.01447587,  0.03438791],
           [ 0.04037998,  0.00761902,  0.01447587,  0.10132225, -0.01048251],
           [-0.02013773,  0.02439148,  0.03438791, -0.01048251,  0.1417496 ]])
    >
    > U,As_out,Ls_out = DFA(X,group,5)
    >
    > U
    array([[-4.17688874, -4.00309392, -4.81969121, -2.2590535 ,  0.09912727],
           [-3.84164699, -4.48421541, -5.32854504, -1.23302594,  3.20454647],
           [-3.81085207, -3.81397856, -8.03905429, -1.78398858,  0.9193002 ],
           [-3.24935377, -4.45386899, -5.43568688, -1.75219293,  1.59185795],
           [-4.13154582, -2.09087065, -6.00844197, -1.77062053,  2.11303517],
           [-2.16978732, -4.9634328 , -6.3567282 , -1.06107911,  1.31295895],
           [-1.5773928 , -2.78409584, -5.3520629 , -2.43949608,  0.93512979],
           [ 0.99791536, -3.22594943, -6.16414955, -2.19322281,  2.13121685],
           [-1.37244426, -5.24757135, -5.99415143, -3.15716437,  1.55257506],
           [-0.69651359, -3.79497195, -5.55893209,  0.07919147,  0.677332  ]])
    >
    """

    # Get B,W
    B, W = __BW__(X, group)

    # produce a diagonal matrix L of generalized
    # eigenvalues and a full matrix A whose columns are the
    # corresponding eigenvectors so that B*A = W*A*L.
    L, A = scipy.linalg.eig(B, W)

    # need to normalize A such that Aout'*W*Aout = I
    # introducing Cholesky decomposition K = T'T
    # (see Seber 1984 "Multivariate Observations" pp 270)
    # At the moment
    # A'*W*A = K so substituting Cholesky decomposition
    # A'*W*A = T'*T ; so, inv(T')*A'*W*A*inv(T) = I
    # & [inv(T)]'*A'*W*A*inv(T) = I thus, [A*inv(T)]'*W*[A*inv(T)] = I
    # thus Aout = A*inv(T)
    K = scipy.dot(transpose(A), scipy.dot(W, A))
    T = scipy.linalg.cholesky(K)
    Aout = scipy.dot(A, scipy.linalg.inv(T))

    # Sort eigenvectors w.r.t eigenvalues
    order = __flip__(scipy.argsort(reshape(L.real, (len(L), ))))
    Ls = __flip__(scipy.sort(L.real))

    # extract & reduce to required size
    As_out = scipy.take(Aout, order[0:nofac].tolist(), 1)
    Ls_out = Ls[0:nofac][scipy.newaxis, :]

    # Create Scores (canonical variates) is the matrix of scores ###
    U = scipy.dot(X, As_out)

    # convert pc-dfa loadings back to original variables if necessary
    if pcloads is not None:
        loads2 = scipy.dot(transpose(pcloads), As_out)
    else:
        loads2 = None

    return U, As_out, Ls_out, loads2
#
#
def DFA_XVAL(RawX, noloads, group, names, mask, nofac, ptype='covar'):
    """Perform DFA with full cross validation
    """

    rx1, rx2, rx3, ry1, ry2, ry3 = (None, None, None, None, None, None)

    if max(mask) > 1:
        (rx1, rx2, rx3,
         ry1, ry2, ry3,
         rn1, rn2, rn3) = __split__(RawX,
                                    scipy.array(group, 'i')[:, scipy.newaxis],
                                    mask[:, scipy.newaxis],
                                    names)
    elif max(mask) < 2:
        (rx1, rx2,
         ry1, ry2,
         rn1, rn2) = __split__(RawX,
                               scipy.array(group, 'i')[:, scipy.newaxis],
                               mask[:, scipy.newaxis],
                               names)

    pcscores, pp, pr, pceigs = PCA_NIPALS(rx1, noloads, kind=ptype)

    # get indices
    idxn = scipy.arange(RawX.shape[0])[:, scipy.newaxis]
    tr_idx = scipy.take(idxn, _index(mask, 0), 0)
    cv_idx = scipy.take(idxn, _index(mask, 1), 0)

    # train
    trscores, loads, eigs, dummy = DFA(pcscores[:, 0:noloads], ry1, nofac)

    # cross validation
    # Get projected pc scores
    rx2 = rx2 - scipy.resize(scipy.mean(rx2, 0), (len(rx2), rx1.shape[1]))
    pcscores = scipy.dot(rx2, transpose(pp))

    cvscores = scipy.dot(pcscores[:, 0:noloads], loads)

    # independent test
    if max(mask) > 1:
        ts_idx = scipy.take(idxn, _index(mask, 2), 0)
        rx3 = rx3 - scipy.resize(scipy.mean(rx3, 0), (len(rx3), rx1.shape[1]))
        pcscores = scipy.dot(rx3, transpose(pp))
        tstscores = scipy.dot(pcscores[:, 0:noloads], loads)

        scores = scipy.zeros((RawX.shape[0], nofac), 'd')

        tr_idx = reshape(tr_idx, (len(tr_idx),)).tolist()
        cv_idx = reshape(cv_idx, (len(cv_idx),)).tolist()
        ts_idx = reshape(ts_idx, (len(ts_idx),)).tolist()
        _put(scores, tr_idx, trscores)
        _put(scores, cv_idx, cvscores)
        _put(scores, ts_idx, tstscores)
    else:
        scores = scipy.concatenate((trscores, cvscores), 0)
        tr_idx = reshape(tr_idx, (len(tr_idx),)).tolist()
        cv_idx = reshape(cv_idx, (len(cv_idx),)).tolist()
        _put(scores, tr_idx, trscores)
        _put(scores, cv_idx, cvscores)

    # get loadings for original variables
    loads = scipy.dot(transpose(pp)[:, 0:noloads], loads)

    return scores, loads, eigs
