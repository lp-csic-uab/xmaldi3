#!/usr/bin/env python
# -*- coding: latin-1 -*-
#
"""
trf 016 (xmaldi 201)
10 october 2008

-->
"""
#
import os
import scipy
import _thread
import warnings
import wx
import logging
from commons.iconic import Iconic
from commons.warns import tell
from xmaldi_panels import SpectrumMaldi
from xmaldi_fcheck import FileCheck
from xmaldi_fselect import FileSelector
from xmaldi_images import ICON
from xmaldi_funcs import norm01, normalize_list
from xmaldi_funcs import get_line_range, load_file
# noinspection PyUnresolvedReferences
from xmaldi_common import Common
from xmaldi_pan_trf import TransPanel
#
log4 = logging.getLogger(__name__)
#
TITLE = ' Data Processor vs.0.16'
#
#
def save_file(data, filepath):
    items = [str(item) for item in data]
    txt = '\n'.join(items)
    log4.info('sfl, Saved file= %s' % filepath)
    open(filepath, 'w').write(txt)
#
def cut(filename):
    """Returns the leftmost part of the file name for graphing purposes"""
    filename = os.path.normpath(filename)

    if filename and len(filename) > 50:
        filename = filename[-50:].split(os.sep, 1)[1]

    if filename is None:
        filename = ''
    return filename
#
def get_min(data, resolution=20):
    """Get minimums in sections of size <resolution>.

    Includes first and last points of the data series
    -data: pairs of x,y values or alternatively only y values

    """
    # if type(ydata[0]) is tuple:
    try:
        ydata = [y for x, y in data]
    except TypeError:
        log4.debug('gmn, TypeError in data')
        ydata = list(data)
    #
    ymins = [ydata[0]]
    xmins = [0]
    #
    total_points = len(ydata)
    if resolution > total_points:
        resolution = total_points
        too_low = 'few points and/or resolution too low'
        log4.warning('gmn, %s' % too_low)
        tell(too_low)
    #
    rang = scipy.linspace(0, total_points, resolution)
    # noinspection PyUnresolvedReferences
    rang = rang.astype(int)
    for i in range(resolution - 1):
        y_sec = ydata[rang[i]:rang[i + 1]]
        minim = min(y_sec)
        index = y_sec.index(minim) + rang[i]
        ymins.append(minim)
        xmins.append(index)
    xmins.append(total_points)      # last point
    ymins.append(ydata[-1])
    return xmins, ymins             # tuple of lists
#
def correct_min(xmins, ymins, data):
    """Correct minimum among sector minimums.

    xmins, [list of x_minimals]
    ymins, [list of y_minimals]
    ydata, [maldi]
    from the minimums prepared made lineal adjustments.
    for each adjustment check if there are values under it.
    then modify.

    """
    # ymin=[max(0,item) for item in ymin]
    try:
        ydata = [y for x, y in data]
    except TypeError:
        ydata = data

    total_min = len(ymins)
    xvalues = []
    yvalues = []
    for pos in range(total_min - 1):
        xpair = xmins[pos:pos + 2]
        ypair = ymins[pos:pos + 2]
        x_range = range(xpair[0], xpair[1])

        # the adjustment is not perfect because the series xpair is artificial,
        # it doesnt correspond with the distribution of points
        f = scipy.polyfit(xpair, ypair, 1)      # linear adjust
        val = scipy.polyval(f, x_range)         # step could be made larger
        #
        rest = ydata[xpair[0]:xpair[1]] - val
        negatives = [item for item in rest if item < 0]
        if len(negatives) > (len(rest) / 100):
            new_xmin = rest.argmin() + xpair[0]
            if new_xmin not in xmins:
                new_ymin = ydata[new_xmin]
                #
                xvalues.append(new_xmin)
                yvalues.append(new_ymin)
    if len(xvalues) > 0:
        for idx in range(len(xvalues) - 1, -1, -1):
            xmins.append(xvalues[idx])
            xmins.sort()
            index = xmins.index(xvalues[idx])
            ymins.insert(index, yvalues[idx])

    return xmins, ymins       # list
#
def adjust_curve(xmins, ymins, sectors=5, degree=5, excess=1000):
    """Build the best fit curve for a series of points.

    From a set of zonal minimums:
        1.-adjust a polynomial of degree 'n' in 'm' sectors
           It adjust the polynomial on a wider range of minimums to
            prevent border effects.
        2.-calculate values in polynomials for each xdata value

    This will be used in subtract_curves() to subtract these values from
     the original maldi

    -xmins, ymins: x,y values of the points to be fitted
    -sectors:
    -degree: degree of the polynomial to fit.
    excess: extension of the sector points relative to the mathematical one to
            prevent border effect in the fitting.

    """
    data_length = xmins[-1]        # total length til last minute
    step = data_length / sectors   # points for step
    #
    minimums = zip(xmins, ymins)
    log4.debug('acv, Data_length: %s' % data_length)
    log4.debug('acv, ...... step: %s' % step)
    min_ini = 0
    yfits = []
    first_sector = True
    shift_start = 0
    for sector in range(sectors):
        ini = min_ini - shift_start
        end = min_ini + step + excess
        #
        xx, yy = zip(*[mini for mini in minimums if ini <= mini[0] < end])
        #
        if first_sector:
            shift_start = excess   # for the next sector...
            first_sector = False
        #
        try:
            f = scipy.polyfit(xx, yy, degree)
        except ValueError:
            return None, None
        #
        item = range(min_ini, min_ini + step)
        yfit = list(scipy.polyval(f, item))
        yfits.extend(yfit)
        min_ini += step

    return yfits    # list
#
def subtract_curves(data, to_subtract):
    """Subtracts two series of numbers point to point.

    Subtract only positive values

    """
    try:
        original = [y for x, y in data]
    except TypeError:
        original = data
    subtract = scipy.array(to_subtract)
    subtract = scipy.maximum(0, subtract)
    spec = scipy.array(original)
    # noinspection PyTypeChecker
    largo = min(len(spec), len(subtract))
    return spec[:largo] - subtract[:largo]
#
def smooth(ydata, window=3):
    """Smooth noise in a series of numbers.

    -window: window for smoothing.

    """
    double_width = window * 2
    num_points = len(ydata)
    y = list(ydata[:window])
    for point in range(num_points - double_width):
        media = scipy.mean(ydata[point:point + double_width])
        y.append(media)
    y.extend(ydata[-window:])
    return y                    # list
#
def compress(ydata, factor=2):
    """factor=factor compression"""
    double_width = factor * 2
    num_points = len(ydata)
    y = []
    for point in range(0, num_points, double_width):
        media = scipy.mean(ydata[point:point + double_width])
        y.append(media)
    x = range(factor, num_points + factor, double_width)
    return x, y
#
#
class TransformerFileSelector(FileSelector):
    """File selector in transformer window"""
    def __init__(self, parent, name='hello', directory=''):
        FileSelector.__init__(self, parent, name)
        self.directory = directory

    def get_buttons(self):
        """Modify for different buttons"""
        return dict(Load=('LOAD', 'on_load'),
                    Avrg=('AVG', 'on_avg'),
                    Del=('DELETE', 'on_delete'),
                    SelAll=('SEL ALL', 'on_sel_all'),
                    DelAll=('DEL ALL', 'on_del_all'),
                    Show=('SHOW', 'on_show')
                    )

    def get_init_dir(self):
        return self.directory

    def set_init_dir(self, directory):
        self.directory = directory
#
#
# noinspection PyUnusedLocal
class Transformer(TransPanel):
    def __init__(self, parent):
        TransPanel.__init__(self, parent)

        self.process = self.rbx_procs.GetStringSelection()
        self.mode = self.rbx_mode.GetStringSelection()

        self.Bind(wx.EVT_BUTTON, self.on_trf, self.bt_trf)
        self.Bind(wx.EVT_RADIOBOX, self.on_process, self.rbx_procs)
        self.Bind(wx.EVT_BUTTON, self.on_zero, self.bt_zero)
        self.Bind(wx.EVT_RADIOBOX, self.on_mode, self.rbx_mode)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.bt_save)

    def on_process(self, evt):
        self.process = self.rbx_procs.GetStringSelection()
        log4.info('ops, Process: %s' % self.process)

    # noinspection PyUnresolvedReferences
    def on_zero(self, evt):
        for radio in ['tc_%i' % number for number in range(1, 6)]:
            getattr(self, radio).SetLabel('0')

    def on_mode(self, evt):
        self.mode = self.rbx_mode.GetStringSelection()
        log4.info('omd, mode: %s' % self.mode)

    def on_trf(self, evt):
        evt.Skip()

    def on_save(self, evt):
        evt.Skip()
#
#
class ProcessFiles(wx.Panel):
    # noinspection PyArgumentList
    def __init__(self, parent, directory=''):
        wx.Panel.__init__(self, parent)
        log4.debug('ini, Process dir: %s' % directory)
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer_lft = wx.BoxSizer(wx.VERTICAL)
        pan_lb = wx.Panel(self, size=(10, 10))
        self.pan_tfs = TransformerFileSelector(self, directory=directory)
        self.bt_check_format = wx.Button(self, -1, 'CheckFormat',
                                         size=(162, 25), pos=(10, 2))
        sizer_lft.Add(self.pan_tfs, 1, wx.EXPAND)
        sizer_lft.Add(self.bt_check_format, 0, wx.ALL, 10)
        sizer_lft.Add(pan_lb, 0, wx.EXPAND)

        self.pan_trf = Transformer(self)

        sizer.Add(sizer_lft, 0, wx.EXPAND)
        sizer.Add(self.pan_trf, 0, wx.EXPAND)

        self.SetSizer(sizer)
        self.Fit()

        self.Bind(wx.EVT_BUTTON, self.on_check_format, self.bt_check_format)
        self.Bind(wx.EVT_LISTBOX_DCLICK, self.on_list)

    def on_list(self, evt):
        dfile = self.pan_tfs.checklist.GetStringSelection()
        self.dfile_path = self.pan_tfs.current_dir + os.sep + dfile
        evt.Skip()

    def on_check_format(self, evt):
        evt.Skip()
#
#
# noinspection PyUnusedLocal
class TransFrame(wx.Frame, Iconic):
    def __init__(self, line_range=None, data=None, directory=''):
        wx.Frame.__init__(self, None, title=TITLE, size=(700, 650))
        Iconic.__init__(self, icon=ICON)
        #
        self.line_range = line_range or ()
        self.data = data or {}
        self.init_dir = directory
        self.Bind(wx.EVT_CLOSE, self.on_close_window)
        #
        self.sptr = wx.SplitterWindow(self, style=wx.SP_THIN_SASH)

        #
        self.prc_fls = ProcessFiles(self.sptr, directory=self.init_dir)
        self.maldi = SpectrumMaldi(self.sptr)
        self.maldi.toolbar.SetBackgroundColour('white')
        self.maldi.mass_txt.SetBackgroundColour('white')
        self.maldi.control.SetBackgroundColour('white')
        #
        self.sptr.SplitHorizontally(self.maldi, self.prc_fls, sashPosition=250)
        #
        self.Bind(wx.EVT_BUTTON, self.on_transform, self.prc_fls.pan_trf.bt_trf)
        self.Bind(wx.EVT_BUTTON, self.on_shelve, self.prc_fls.pan_trf.bt_save)
        self.Bind(wx.EVT_LISTBOX_DCLICK, self.on_list)
        self.Bind(wx.EVT_BUTTON, self.on_show,
                  self.prc_fls.pan_tfs.instances['Show'])
        self.Bind(wx.EVT_BUTTON, self.on_avg,
                  self.prc_fls.pan_tfs.instances['Avrg'])
        self.Bind(wx.EVT_BUTTON, self.on_check_format,
                  self.prc_fls.bt_check_format)
        self.maldi.toolbar.update()
        #
        # noinspection PyArgumentList
        self.sptr.SplitHorizontally(self.maldi, self.prc_fls, 0)
        self.sptr.SetMinimumPaneSize(1)

        if os.path.isfile(self.init_dir):
            log4.debug('ini, Is file')
            self.read_and_plot(self.init_dir)
            self.filename = self.init_dir
        elif os.path.exists(Common.demo_file):
            log4.debug('ini, Is not file')
            self.data = {'column_y': 1, 'column_x': None, 'separator': '\t'}
            self.filename = Common.demo_file
            self.read_and_plot(self.filename)

        warnings.simplefilter('ignore', scipy.RankWarning)
    #
    def on_check_format(self, evt):
        fcheck = FileCheck(self, init_dir=self.init_dir)
        fcheck.Show()
    #
    def set_data(self, line_range, data, init_dir):
        """Sets global attributes for reading maldi data from file.

        Called from a FileCheck instance by self.parent.set_data
        - line_range = (line_ini, line_end)
        - data: dictionary with info for file reading
              {column:self.column,
               separator:self.sep)
        - init_dir: self.init_dir

        This function works ok in init_win.
        Values are passed to Maldi module and we work from there.
        The problem here is that check_format values are read from set_data
        but then we read also with get_line_range in main that works with
        Maldi.offset.
        get_line_range produces (1,len(archivo)) and then an error is produced
        when I need to read from line > 1.
        Could I pass the new values to SpecMaldi in order to be read by
        Maldi.offset and be transmitted to getlines???
        self.maldi.set_offset(line_ini=0, line_end=0) could be the answer

        """
        #
        self.line_range = line_range
        self.data = data
        self.init_dir = init_dir
        self.maldi.set_offset(line_range[0], line_range[1])
        self.prc_fls.pan_tfs.set_init_dir(init_dir)
        #
        log4.debug('gdt, Got1, lines= %s\n'
                   '     got2, data= %s\n'
                   '     got3, dir= %s\n'
                   % (self.line_range, self.data, self.init_dir))

    def on_shelve(self, evt):
        """Process and save resulting files.

        Accepts three modes:
        - averaged file:
             Averages all files selected and save them without any processing.
             Not sure if this is what it should be. Maybe save them averaged
             after processing ?
        - individually selected
             Applies selected transformations to a group of selected files and
             saves the resulting files
        - file tree
             Reads all files in a source folder, applying transformation to
             each of them and saving them in a target folder reproducing the
             source folder structure

        """
        self.init_params()
        self.dfile_paths = self.prc_fls.pan_tfs.get_files_selected()
        filepath = None
        basedir = ''
        try:
            mode = self.prc_fls.pan_trf.mode
        except AttributeError:          # there is no mode selected
            tell('Select Saving Mode')
            return
        #
        if mode == 'averaged file':
            dialog = wx.FileDialog(self,
                                   "Create/Select file name", self.init_dir)
            if dialog.ShowModal() == wx.ID_OK:
                filepath = dialog.GetPath()
            dialog.Destroy()
            if not filepath:
                tell('Not file selected')
                return
            #
            data, number, out = self.do_avg()
            if data is None:
                return
            #
            save_file(data, filepath)
            log4.info('osv, Averaged filepath: %s' % filepath)

        elif mode == 'individual files':
            if not self.dfile_paths:
                tell('No files selected\n   nut!  ;-(')
                return
            dialog = wx.DirDialog(self,
                                  "Create/Select Folder", self.init_dir)
            if dialog.ShowModal() == wx.ID_OK:
                basedir = dialog.GetPath()
            dialog.Destroy()
            if not basedir:
                tell('No folder selected')
                return
            # noinspection PyUnboundLocalVariable
            log4.info('osv, Individual basedir: %s' % basedir)

            for dfile_path in self.dfile_paths:
                self.spectrum = self.read_file(dfile_path, trimming=0)
                if self.spectrum is None:
                    continue
                self.ydata = list(self.spectrum)
                combination = self.get_tasks()
                self.do_job(combination)
                filename = os.path.basename(dfile_path)
                dfile_path = basedir + os.sep + filename
                save_file(self.ydata, dfile_path)

        elif mode == 'file tree':
            dialog = wx.DirDialog(self, "Select Source Folder",
                                  self.init_dir)
            if dialog.ShowModal() == wx.ID_OK:
                source_dir = dialog.GetPath()
                log4.info('osv, Tree source_dir: %s' % source_dir)
            dialog.Destroy()
            #
            dialog = wx.DirDialog(self, "Select/Create Target Folder",
                                  self.init_dir)
            if dialog.ShowModal() == wx.ID_OK:
                target_dir = dialog.GetPath()
                log4.info('osv, Tree target_dir= %s' % target_dir)
            dialog.Destroy()
            #
            # noinspection PyUnboundLocalVariable
            _thread.start_new_thread(self.threaded_process_dirs,
                                     (source_dir, target_dir))
    #
    def threaded_process_dirs(self, source_dir, target_dir):
        """"""
        for root, dirs, dfiles in os.walk(source_dir):
            log4.debug('tpd, root: %s' % root)
            extra_dir = root.replace(source_dir, '')
            full_dir = target_dir + extra_dir
            if extra_dir != '':
                os.mkdir(full_dir)
            #
            combination = self.get_tasks()

            for dfile in dfiles:
                dfile_path = root + os.sep + dfile
                self.spectrum = self.read_file(dfile_path)
                if self.spectrum is None:
                    continue
                self.ydata = list(self.spectrum)
                self.do_job(combination)
                dfile_path = full_dir + os.sep + dfile
                save_file(self.ydata, dfile_path)
                log4.debug('tpd, Processed threaded %s' % dfile)

    def on_list(self, evt):
        """"""
        self.filename = self.prc_fls.dfile_path
        log4.debug('olt, Selected on list: %s' % self.filename)
        self.read_and_plot(self.filename)

    def on_avg(self, evt):
        """"""
        self.dfile_paths = self.prc_fls.pan_tfs.get_files_selected()
        if not self.dfile_paths:
            return
        avg_spectrum, number, out = self.do_avg()
        if avg_spectrum is None:
            return
        label = 'Add %i, %s files' % (number, out)
        self.plot_spectrum(avg_spectrum, filename=label)
        self.spectrum = avg_spectrum
        self.last_name_plotted = label

    # noinspection PyUnboundLocalVariable
    def do_avg(self):
        """Averages spectra.

        This code is nearly the same as the one used in threaded_on_avg in
        xmaldi_panels (ln 1111). Could it be generalized ?.

        """
        log4.debug('dav, Start averaging data')

        n = 0
        out = 0
        if self.maldi.get_maldi_range_changed():
            self.maldi.clean()
        lines = get_line_range(self.dfile_paths[0], Common)
        log4.debug('trf_533_da, lines to read from %s - %s' % lines)
        for dfile in self.dfile_paths:
            # don't normalize
            spec = load_file(dfile, lines, self.data, check_size=False)
            #
            if not spec:
                out += 1
                continue
            #
            n += 1
            x = scipy.array(spec)
            try:
                a = a + x
            except NameError:
                a = x
        #
        try:
            # noinspection PyAugmentAssignment
            a = a / n
        except UnboundLocalError:       # p.e. "a referenced before assignment"
            tell(("Major file reading error\n"
                  "Try selecting in the visor\n"
                  "a smaller \"ini-end\" range"))

            return None, None, None
        #
        out_text = ' (%s out)' % str(out) if out else ""
        #
        return a, n, out_text

    def on_show(self, evt):
        """Draw spectra from files checked in file selector.

        Erases previously draw spectra.
        It is not clear how to set self.filepath in order to apply
        transformations to all or maybe only one of the spectra selected.

        """
        self.dfile_paths = self.prc_fls.pan_tfs.get_files_selected()
        if not self.dfile_paths:
            return
        if self.maldi.get_maldi_range_changed():
            log4.debug('osw, Maldi_range_changed')
            self.maldi.clean()
        lines = get_line_range(self.dfile_paths[0], Common)
        #
        for dfile_path in self.dfile_paths:
            dfile = os.path.basename(dfile_path)
            spectrum = load_file(dfile_path, lines, self.data, check_size=False)
            if not spectrum:
                continue
            if Common.normalize:
                spectrum = normalize_list(spectrum)
            self.plot_spectrum(spectrum, filename=dfile, clean=False)
            self.spectrum = spectrum
            self.last_name_plotted = dfile

    def on_transform(self, evt):
        self.transform(True)

    # noinspection PyCallingNonCallable
    def do_job(self, combinations):
        """"""
        functions = ['baseline', 'smooth', 'compress', 'normalize']
        for work in combinations:
            # noinspection PySimplifyBooleanCheck
            if work != 0:
                f = getattr(self, functions[work - 1])
                self.ydata = f()

    def get_tasks(self):
        """
        if not combined, takes self.task and puts it in
        the task list to be sent automatically to do_job
        """
        combinations = []
        pan_trf = self.prc_fls.pan_trf
        combine = pan_trf.cbx_combine.IsChecked()
        if combine:
            works = ['tc_1', 'tc_2', 'tc_3', 'tc_4', 'tc_5']
            for index, work in enumerate(works):
                try:
                    # noinspection PyUnresolvedReferences
                    doit = int(getattr(pan_trf, work).GetValue())
                except ValueError:
                    doit = 0
                combinations.append(doit)
        else:
            # self.task label starts with a number
            doit = int(pan_trf.process.split()[0])
            combinations.append(doit)
        #
        return combinations

    def init_params(self):
        """"""
        pan_trf = self.prc_fls.pan_trf
        self.resolution = int(pan_trf.tc_rsln.GetValue())
        self.degree = int(pan_trf.tc_dgr.GetValue())
        self.sectors = int(pan_trf.tc_sectors.GetValue())
        self.window = int(pan_trf.tc_smooth.GetValue())
        self.factor = int(pan_trf.tc_factor.GetValue())

    def transform(self, refresh=False, fromfile=False):
        """Modify spectra using one or more transformations.

        refresh: clean figure if True
        fromfile: read from self.filename, otherwise use current self.spectrum

        """
        if refresh:
            self.maldi.clean()
        #
        self.init_params()
        #
        log4.debug('trm, In transform')
        name = '%s_processed' % self.last_name_plotted
        #
        if fromfile:
            try:
                log4.debug('trm, Transform %s' % self.filename)
                self.spectrum = self.read_file(self.filename)
                name = '%s_processed' % self.filename
            except AttributeError:
                log4.warning('trm, AttributeError')
                # self.spec = self.read_file()
        #
        combine = self.prc_fls.pan_trf.cbx_combine.IsChecked()
        #
        try:
            ydata = [y for x, y in self.spectrum]
            xdata = [x for x, y in self.spectrum]
            log4.debug('trm, Maldi x,y: %s ...' % str(xdata[:10]))
        except TypeError:
            xdata = None
            ydata = self.spectrum[:]
            log4.debug('trm, Only ydata: %s ...' % str(ydata[:10]))
        #
        y = None
        if combine:
            task = ''
            combination = self.get_tasks()
            self.ydata = ydata[:]
            self.do_job(combination)
            y = self.ydata
        else:
            try:
                task = self.prc_fls.pan_trf.process
            except AttributeError:
                tell("""    Select a task\nor task combination\n
                :-|""")
                return
            #
        if self.prc_fls.pan_trf.cbx_ori.IsChecked():
            self.plot_spectrum(self.spectrum, 'original')   # re-draw selection
        #
        if 'baseline' in task:
            log4.info('trm, Baseline %s ...' % str(self.spectrum[:10]))
            xx, yy = get_min(self.spectrum, self.resolution)
            xx, yy = correct_min(xx, yy, self.spectrum)
            xx, yy = correct_min(xx, yy, self.spectrum)
            if self.prc_fls.pan_trf.cbx_lwr.IsChecked():
                self.maldi.axes.plot(xx, yy, 'ro')      # draw line of minimums
            curve = adjust_curve(xx, yy, self.sectors, self.degree)
            y = subtract_curves(self.spectrum, curve)
            if y is None:
                return
            if not xdata:
                xdata = range(len(curve))
            if self.prc_fls.pan_trf.cbx_pln.IsChecked():
                yfit_l = scipy.array(curve)
                self.maldi.axes.plot(xdata, yfit_l)      # draw adjusting line
        #
        if 'normalize' in task:
            y_array = scipy.array(ydata)
            a = y_array[:, scipy.newaxis]
            a = a.swapaxes(0, 1)
            a = norm01(a)
            y = a[0]
            y = list(y)
        #
        if 'smooth' in task:
            y = smooth(ydata, self.window)
        #
        if 'compress' in task:
            xc, y = compress(ydata, self.factor)
            self.maldi.axes.plot(xc, y)
        #
        if refresh and y is not None:
            if xdata:
                y = zip(xdata, y)
            self.plot_spectrum(y, filename=name)  # plot maldi final
            self.maldi.canvas.draw()

    def baseline(self):
        """Adjust spectrum baseline."""
        xx, yy = get_min(self.ydata, self.resolution)
        xx, yy = correct_min(xx, yy, self.spectrum)
        xx, yy = correct_min(xx, yy, self.spectrum)
        #
        curve = adjust_curve(xx, yy, self.sectors, self.degree)
        y = subtract_curves(self.ydata, curve)
        return list(y)

    def smooth(self):
        y = smooth(self.ydata, self.window)
        return list(y)

    def compress(self):
        xc, y = compress(self.ydata, self.factor)
        return list(y)

    def normalize(self):
        np_y = scipy.array(self.ydata)
        a = np_y[:, scipy.newaxis]
        a = a.swapaxes(0, 1)
        a = norm01(a)
        y = a[0]
        return list(y)
    #
    def adjust_baseline(self, ydata, sample=False):
        """"""
        xx, yy = get_min(ydata, self.resolution)
        if sample:
            self.maldi.axes.plot(xx, yy)     # draws minimal line
        curve = adjust_curve(xx, yy, self.sectors, self.degree)
        rest = subtract_curves(self.spectrum, curve)
        return rest, curve
    #
    def read_and_plot(self, dfile_path):
        """"""
        spectrum = self.read_file(dfile_path)
        #
        if spectrum is None:
            tell('   Illegible file  :-P\nTry check format')
            return
        #
        self.plot_spectrum(spectrum, dfile_path)
        self.spectrum = spectrum
        self.last_name_plotted = dfile_path
    #
    def read_file(self, dfile_path, trimming=10):
        """Loads a range of lines from  a file.

        It reads one or two columns depending of the maldi type

        """
        lines = get_line_range(dfile_path, Common)
        log4.debug('rfl, Reading lines %i - %i from %s'
                   % (lines[0], lines[1], dfile_path))
        return load_file(dfile_path, lines, self.data,
                         check_size=False, trimming=trimming)
    #
    def plot_spectrum(self, spectrum, filename='noname', hold=True, clean=True):
        """Plot spectrum and labels.

        clean : if True, return axes to full size.
         plot_spectrum method here is identical to method in MaldiPanel...

        """
        try:
            masses, intensities = zip(*spectrum)
        except (ValueError, TypeError):
            masses = range(len(spectrum))
            intensities = spectrum

        lines = self.maldi.axes.get_lines()
        color = 'b'
        filename = cut(filename)
        #
        if len(lines) < 2:
            clean = True   # ???
        #
        if not clean:
            # keep current range
            self.maldi.xlim = self.maldi.axes.viewLim.intervalx
            self.maldi.ylim = self.maldi.axes.viewLim.intervaly
        #
        self.maldi.axes.plot(masses, intensities)
        #
        # set offsets in control widget
        control = self.maldi.control
        #
        control.data_ini.Clear()
        control.data_fin.Clear()
        offset = self.maldi.get_offset()              # how works as main?
        control.data_ini.WriteText(str(offset[0]))
        control.data_fin.WriteText(str(offset[1]))
        #
        # get matplotlib lines already drawn
        lines = self.maldi.axes.get_lines()
        #
        try:
            color = lines[-1].get_color()
        except IndexError:
            log4.warning('psp, Index error')
        #
        # determine position for label of new line
        max_lines = 10
        if len(lines) < max_lines:
            ypos = 0.9 - (0.06 * len(lines))
            name = '--- %s' % filename
        else:
            ypos = 0.9 - (0.06 * max_lines)
            name = '--- more than %i' % max_lines
        #
        self.maldi.figure.text(0.6, ypos, name, fontsize=7, color=color)
        self.maldi.dibuja(clean)

    def on_close_window(self, evt):
        self.Destroy()


if __name__ == '__main__':
    #
    import logging.config
    logging.config.fileConfig(Common.log_file)
    log4 = logging.getLogger('main')
    log4.info('test_info')
    log4.critical('test_critical')
    #
    demo_dir = os.path.join(os.getcwd(), 'test')
    demo_file = os.path.join(demo_dir, "J8H_0001.dat.out.txt")
    #
    app = wx.App()
    frame = TransFrame(directory=demo_file)
    frame.Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
