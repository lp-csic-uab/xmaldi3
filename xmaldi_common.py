#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
"""
common (xmaldi 201)
10 october 2009
"""
from commons.images import LAMA
import base64
import wx
from io import BytesIO

class LamaBmp:
    """Provides a bitmap from the LAMA image string"""
    def __init__(self):
        img = base64.decodebytes(bytes(LAMA, 'utf-8'))
        fhandle = BytesIO(img)
        bmp = wx.Image(fhandle, wx.BITMAP_TYPE_ANY)
        self.bmp = bmp.ConvertToBitmap()


class Common:
    """Class for transporting global data all around."""

    offset = [1, 1]         # eliminated part, reading range
    offset_temp = [1, 1]
    original = (1, 1)       # holds the value read in init_win.
    init_dir = ""           # initial directory
    range_changed = False
    normalize = False
    #
    # Logging
    log_file = 'logconfig.conf'
    # Files
    data_maldi_dir = 'C:/data_maldi'
    demo_file = ''
#
#
collection = {}
#
#
TEXT_PYCHEM = ('        Hail Dalai Lama and he will give you two files:\n\n'
               '        - C:\data_maldi\save2.txt allows injection of data tables\n'
               '           into PyChem\n'
               '        - C:\data_maldi\save3.cvs an excel compatible file with 3\n'
               '           comma-separated columns:\n'
               '                col1: file full name\n'
               '                col2: code alphanumeric (a0,...,zn)\n'
               '                col3: class code (1-n)\n'
               '        The columns col2 and col3 have to be sent to PyChem\n'
               '        using cut/paste because their import doesnt work\n')


if __name__ == '__main__':
    app = wx.App()
    LamaBmp()
    app.MainLoop()
