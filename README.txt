Xmaldi vs 2.3.0

Xmaldi is a tool for viewing collections of MALDI spectra and perform PCA and
 DFA analyses on them.


Table Of Contents:
------------------
FEATURES
INSTALLATION
REQUIREMENTS
FROM INSTALLER
FROM SOURCE
POST-INSTALL
DOWNLOAD
CHANGE-LOG
TO-DO
CONTRIBUTE


FEATURES
-----------

XMALDI MAIN ANALYSIS WINDOW
Reads and displays spectra from csv files.
Display individual or averaged spectra.
Display heat maps of spectra.
Perform PCA on spectrum collections showing graphs of PCi vs PCj
Perform DFA on spectrum collections showing graphs of PCi vs PCj
Shows 3D views of PCA distribution
Shows loads
Export data collections in a format that can be injected on PyChem.

XMALDI DATA PROCESSOR
Reads and displays spectra from csv files.
Performs baseline adjust, smoothing, compression and normalization of spectra.
Averages spectra.
Saves transformed data for further analysis with xmaldi.


INSTALLATION
------------
Tested in Windows 7 and Windows 10 64 bits.

REQUIREMENTS
-------------
- GnuPlot: Xmaldi uses GnuPlot for 3D representation of PCA and DFA distributions.
     -Download GnuPlot installer (ex. gp522-win64-mingw.exe) from
           http://sourceforge.net/projects/gnuplot/files/gnuplot/
     -Execute the installer and install on i.e. C:\gnuplot.
     -Put gnuplot\bin in your PATH environmental variable.


From Installer
Download Xmaldi_x.y_setup.exe windows installer from repository at the Xmaldi
 project download page.
Get your Password for the installer.
Double click on the installer and follow the Setup Wizard.
Run Xmaldi by double-clicking on the Xmaldi short-cut in your desktop or from
 the START-PROGRAMS application folder created by the installer.

From Source
Install Python and third party software indicated in Dependencies.
Download Xmaldi source code from its Mercurial Source Repository.
Download commons source code from its Mercurial Source Repository.
Copy the folders in python site-packages or in another folder in the python path.
Run Xmaldi by double-clicking on the kblast_main.pyw module.

Source Dependencies:
Python 3.6 and above (not tested with other versions)
wxPython 4.0.0b2 and above
gnuplot.py
commons (from LP CSIC/UAB Google Code repository)
Third-party program versions correspond to those used for the installer available
 here. Lower versions have not been tested, although they may also be fine.


Post-Install

Before starting any work with the application ...

On a fresh installation, the first time Xmaldi is executed a directory named
 C:\datos_maldi is created. In this directory you can find the following files:


DOWNLOAD
-----------
 You can download the last version of Xmaldi here.

After downloading the binary installer, you have to e-mail us at
 to get your free password and unlock the installation program.
 The password is not required to run the application from source code


CHANGELOG
-------------
2.03 (to be released)
  * Migrated to python 3.6
  * Bitbucket repository
  * Q5 is not available anymore

2.01 December 2010
  * Location on mercurial
1.10 June 2007
  * Release executable xmaldi\_app\_110.exe working on Python 2.5
0.90 April 2007
  * Release executable maldi_gui_90.exe working on Python 2.5

TODO
--------
Reference PyChem code in docs and wherever needed.
Simplify/extend PyChem functions with dedicated PCA library.

CONTRIBUTE
-----------
These programs are made to be useful. If you use them and don't work entirely
 as you expect, let us know about features you think are needed, and we will
 try to include them in future releases.


See LICENCE.txt for details about licensing of the software (GPL v3).