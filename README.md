
---

**WARNING!**: This is the *Old* source-code repository for Xmaldi3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/xmaldi3/) located at https://sourceforge.net/p/lp-csic-uab/xmaldi3/**  

---  
  
  
![https://lh3.googleusercontent.com/-lsMwNHXINHo/USTeZoTIeZI/AAAAAAAAAUY/dvkJ6BrDsVU/s144/xmaldi.png](https://lh3.googleusercontent.com/-lsMwNHXINHo/USTeZoTIeZI/AAAAAAAAAUY/dvkJ6BrDsVU/s144/xmaldi.png)


---

**WARNING!**: This is the *Old* source-code repository for Xmaldi3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/xmaldi3/) located at https://sourceforge.net/p/lp-csic-uab/xmaldi3/**  

---  
  

**Table Of Contents:**

[TOC]

#### Description

**`Xmaldi`** is a tool for viewing collections of MALDI spectra and perform PCA and DFA analyses on them.

![https://lh4.googleusercontent.com/-cpSybYzxEls/USTeZpdl0fI/AAAAAAAAAUg/QPv5-5sssIg/s800/xmaldi_full_view.png](https://lh4.googleusercontent.com/-cpSybYzxEls/USTeZpdl0fI/AAAAAAAAAUg/QPv5-5sssIg/s800/xmaldi_full_view.png)

#### Features

XMALDI MAIN ANALYSIS WINDOW

  * Reads and displays spectra from csv files.
  * Display individual or averaged spectra.

![https://lh6.googleusercontent.com/-tbdSGhxPG6c/USTeaLWNpdI/AAAAAAAAAUk/sPSHmt36oy8/s800/xmaldi_main.png](https://lh6.googleusercontent.com/-tbdSGhxPG6c/USTeaLWNpdI/AAAAAAAAAUk/sPSHmt36oy8/s800/xmaldi_main.png)

  * Display heat maps of spectra.
  * Perform PCA on spectrum collections showing graphs of PCi vs PCj
  * Perform DFA on spectrum collections showing graphs of PCi vs PCj

![https://lh3.googleusercontent.com/-64FH1hNXmzE/USTeaoOsWkI/AAAAAAAAAUs/8pZCLZc7UJ8/s800/xmaldi_views_pca_dfa.png](https://lh3.googleusercontent.com/-64FH1hNXmzE/USTeaoOsWkI/AAAAAAAAAUs/8pZCLZc7UJ8/s800/xmaldi_views_pca_dfa.png)

  * Shows 3D views of PCA distribution
  * Shows loads
  * Export data collections in a format that can be injected on `PyChem`.

XMALDI DATA PROCESSOR

  * Reads and displays spectra from csv files.
  * Performs baseline adjust, smoothing, compresion and normalization of spectrum data.
  * Averages spectra.
  * Saves transformed data for further analysis with xmaldi.

![https://lh4.googleusercontent.com/-iN7T0XbNbPo/USTeZsNZuXI/AAAAAAAAAUc/mQCDnWiJ-xQ/s800/Processor.png](https://lh4.googleusercontent.com/-iN7T0XbNbPo/USTeZsNZuXI/AAAAAAAAAUc/mQCDnWiJ-xQ/s800/Processor.png)

#### Installation

Tested in Windows 7 and Windows 10,  64 bits.

##### Requirements

  * [GnuPlot](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/release/): `Xmaldi` uses `GnuPlot` for 3D representation of PCA and DFA distributions.
    1. Download `GnuPlot` installer (ex. [gp522-win64-mingw.exe](https://sourceforge.net/projects/gnuplot/files/gnuplot/5.2.2/gp522-win64-mingw.exe/download)) from http://sourceforge.net/projects/gnuplot/files/gnuplot/
    1. Execute the installer and install on i.e. **C:\gnuplot.**
    1. Put gnuplot\bin in your PATH environmental variable.


##### From Installer

  1. Download `Xmaldi_x.y_setup.exe` windows installer from repository at the [Xmaldi project download page](https://bitbucket.org/lp-csic-uab/xmaldi/downloads).
  1. [Get your Password](#markdown-header-download) for the installer.
  1. Double click on the installer and follow the Setup Wizard.
  1. Run `Xmaldi` by double-clicking on the `Xmaldi` short-cut in your desktop or from the START-PROGRAMS application folder created by the installer.

##### From Source

  1. Install Python and third party software indicated in [Dependencies](#markdown-header-source-dependencies).
  1. Download `Xmaldi` source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/xmaldi3/src).
  1. Download commons source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/commons3/src).
  1. Copy the folders in python site-packages or in another folder in the python path.
  1. Run `Xmaldi` by double-clicking on the `xmaldi_main.pyw` module.

###### _Source Dependencies:_

  * [Python](http://www.python.org/) 3.6 and above (not tested with other versions)
  * [wxPython](http://www.wxpython.org/) 4.0.0b2 and above
  * [gnuplot.py-py3k](https://github.com/oblalex/gnuplot.py-py3k) (At vs 2102 needs to modify gp_win32.py to use gnuplot.exe instead of pgnuplot.exe)
  * [commons](https://bitbucket.org/lp-csic-uab/commons3/src) (from LP CSIC/UAB BitBucket [repository](https://bitbucket.org/lp-csic-uab/commons3/src))

Third-party program versions correspond to those used for the installer available here. Lower versions have not been tested, although they may also be fine.

##### Post-Install

Before starting any work with the application ...

On a fresh installation, the first time `Xmaldi` is executed a directory named **C:\datos\_maldi** is created. In this directory you can find the following files:

  * .....

#### Download

![https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg](https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg) You can download the last version of **`Xmaldi`** [here](https://bitbucket.org/lp-csic-uab/xmaldi/downloads).

After downloading the binary installer, you have to e-mail us at ![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png) to get your free password and unlock the installation program. The password is not required to run the application from source code

#### Change-Log

2.03 (to be released)

  * Migrated to python 3.6
  * Bitbucket repository
  * Q5 is not available anymore

2.01 December 2010

  * Location on mercurial

1.10 June 2007

  * Release executable xmaldi\_app\_110.exe working on Python 2.5

0.90 April 2007

  * Release executable maldi\_gui\_90.exe working on Python 2.5

#### To-Do

  * Reference `PyChem` and Q5 code in docs and wherever needed.
  * Simplify/extend `PyChem` functions with dedicated PCA library.
  * and eventually remove Q5/R dependency

#### Contribute

These programs are made to be useful. If you use them and don't work entirely as you expect, let us know about features you think are needed, and we will try to include them in future releases.

![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png)


---

**WARNING!**: This is the *Old* source-code repository for Xmaldi3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/xmaldi3/) located at https://sourceforge.net/p/lp-csic-uab/xmaldi3/**  

---  
  
