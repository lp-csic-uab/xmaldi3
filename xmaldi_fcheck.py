#!/usr/bin/env python
# -*- coding: latin-1 -*-
#
"""
fcheck 106 (xmaldi 201)
10 october 2009

-->
"""
#
TITLE = '  Data Selector vs.1.06'
#
import wx
import logging
from xmaldi_class_fcheck import BaseFileCheck
from commons.iconic import Iconic
from xmaldi_images import ICON
#
#
log5 = logging.getLogger(__name__)
#
#
class FileCheck(BaseFileCheck, Iconic):
    """
    Must open a file, show contents and allow selection on lines and columns
    to be extracted into a file ready to be read by other programs
    """
    # noinspection PyUnresolvedReferences
    def __init__(self, parent, init_dir=r'C:\ '):
        BaseFileCheck.__init__(self, None, title=TITLE, size=(200, 500))
        Iconic.__init__(self, icon=ICON)
        self.parent = parent
        self.line_ini = -1
        self.line_end = -1
        self.column = 1
        self.sep = '\t'
        self.init_dir = init_dir
        self.lines = []
        #
        window = {'line_ini': ('1', True),
                  'line_end': ('1', ''),
                  'col': ('1', True)}
        #
        for item, value in window.items():
            try:
                getattr(self, 'tc_' + item).SetValue(value[0])
                getattr(self, 'cbx_' + item).SetValue(value[1])
            except AttributeError:
                pass
        #
        header_text = ("  Select a data file\n"
                       "  Select start/end lines\n"
                       "  Select a column separator\n"
                       "  Select column with data\n"
                       )
        #
        self.sep_dict = {'tab': '\t', 'space': ' ', 'scolon': ';',
                         'comma': ',', 'bar': '|', 'dot': '.'}

        self.cb_sep.SetItems(list(self.sep_dict.keys()))
        self.cb_sep.SetValue('tab')
        self.lb_head.SetLabel(''.join(header_text))
        #
        self.tc_ini.SetEditable(False)
        self.tc_end.SetEditable(False)
        self.tc_col.SetEditable(False)
        #
        self.tc_data.Bind(wx.EVT_LEFT_DOWN, self.select_pos)
        self.Bind(wx.EVT_BUTTON, self.on_select_file, self.bt_sfile)
        self.Bind(wx.EVT_BUTTON, self.on_bt_ini, self.bt_ini)
        self.Bind(wx.EVT_BUTTON, self.on_bt_end, self.bt_end)
        self.Bind(wx.EVT_BUTTON, self.on_bt_col, self.bt_col)
        self.Bind(wx.EVT_BUTTON, self.on_ok, self.bt_ok)
    #
    # noinspection PyUnusedLocal
    def on_bt_ini(self, evt):
        """Change 'editability' of end_row text control"""
        if self.tc_ini.IsEditable():
            self.tc_ini.SetEditable(False)
            self.bt_ini.SetLabel('edit')
        else:
            self.tc_end.SetEditable(False)
            self.tc_ini.SetEditable(True)
            self.bt_ini.SetLabel('set')
    #
    # noinspection PyUnusedLocal
    def on_bt_end(self, evt):
        """Change 'editability' of end_row text control"""
        if self.tc_end.IsEditable():
            self.tc_end.SetEditable(False)
            self.bt_end.SetLabel('edit')
        else:
            self.tc_ini.SetEditable(False)
            self.tc_end.SetEditable(True)
            self.bt_end.SetLabel('set')
    #
    # noinspection PyUnusedLocal
    def on_bt_col(self, evt):
        """Change 'editability' of end_row text control"""
        if self.tc_col.IsEditable():
            self.tc_col.SetEditable(False)
            self.bt_col.SetLabel('edit')
        else:
            self.tc_col.SetEditable(True)
            self.bt_col.SetLabel('set')
    #
    def get_sep(self):
        """Reads column delimiter"""
        sep_key = self.cb_sep.GetValue()
        self.sep = self.sep_dict[sep_key]
    #
    # noinspection PyUnusedLocal
    def on_ok(self, evt):
        """Accepts data and returns to parent application.

        this window must determine and pass:
           - column delimiter ('\t',...)
           - column to consider.
           - initial line.
           - file directory.

        """
        self.get_sep()
        if self.line_ini == -1:
            self.line_ini = 1

        end = self.tc_end.GetValue()
        self.line_end = int(end.strip())
        line_range = (self.line_ini, self.line_end)
        kwargs = {'column_y': self.column,
                  'column_x': None,
                  'separator': self.sep}
        #
        self.parent.set_data(line_range, kwargs, self.init_dir)
        self.Destroy()
    #
    def select_pos(self, evt):
        """
        needed for text cursor of text_control to respond to mouse and for
        resending the signal for position determination.

        """
        evt.Skip()
        wx.CallAfter(self.on_select_pos)
    #
    # noinspection PyUnusedLocal
    def on_select_file(self, evt):
        """draw text file"""
        fileopen = ''
        log5.debug('osf, starting init fir: %s', self.init_dir)
        dialog = wx.FileDialog(None, message="Choose a File",
                               defaultDir=self.init_dir)
        if dialog.ShowModal() == wx.ID_OK:
            fileopen = dialog.GetPath()
        dialog.Destroy()
        #
        self.init_dir = fileopen
        try:
            self.lines = open(fileopen, 'r').readlines()
        except IOError:
            return
        total = len(self.lines)
        text = ''.join(self.lines)
        self.tc_data.Clear()
        self.tc_data.SetValue(text)
        self.line_end = total
        self.tc_end.SetValue(str(total))
        log5.debug('osf, first lines %s', text[:20].replace('\n', ', '))
    #
    def on_select_pos(self):
        """Determines initial line and column for file reading"""
        pos = self.tc_data.GetInsertionPoint()
        # xypos is tuple (True, posx , posy)
        xypos = self.tc_data.PositionToXY(pos)

        try:
            line = self.lines[xypos[2]]
        except IndexError:
            return
        #
        self.get_sep()
        line = [x.strip() for x in line.split(self.sep)]
        column = 1
        tot = 0
        for item in line:
            tot += len(item)
            if tot > xypos[1] - 1:
                break
            else:
                column += 1
        #
        if self.tc_end.IsEditable():
            self.line_end = xypos[2] + 1
            self.tc_end.SetValue(str(self.line_end))
        if self.tc_col.IsEditable():
            self.column = column
            self.tc_col.SetValue(str(self.column))
        if self.tc_ini.IsEditable():
            self.line_ini = xypos[2] + 1
            self.tc_ini.SetValue(str(self.line_ini))


if __name__ == '__main__':

    import logging.config
    from xmaldi_common import Common
    logging.config.fileConfig(Common.log_file)
    log5 = logging.getLogger(__name__)

    class MockParent:
        def set_data(self, *args):
            log5.debug("sdt, data received %s", str(args))

    app = wx.App()
    mock_parent = MockParent()
    FileCheck(mock_parent, init_dir='test').Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
