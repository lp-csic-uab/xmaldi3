#!/usr/bin/env python
# -*- coding: latin-1 -*-
#
"""
funcs (xmaldi 2.3.0)
24 january 2018

-->
"""
#
import os
import scipy
import time
import logging
from commons.warns import tell
#
#
log3 = logging.getLogger(__name__)
#
#
def save_table(matrix=None, collection=None, directory=''):
    """Rearrange  data and save datafiles for pychem analysis

    Creates two files:
        save2.txt : intensity data
        save3.txt : three columns:
                    a) spectra names in the order the array is prepared.
                    b) id code
                    c) class/group it belongs

    """
    collection = collection or {}
    matrix = matrix or {}
    from string import ascii_letters as letters
    spectra = []
    points = None
    dfiles = []
    klasses = []
    names = []
    #
    for index, group in enumerate(matrix):
        if not points:
            points = len(matrix[group][0])
        group_dfiles = collection[group]
        klasses.extend([index + 1] * len(group_dfiles))
        dfiles.extend(group_dfiles)
        group_name = [letters[index] + str(idx) for
                      idx in range(len(group_dfiles))]
        names.extend(group_name)
        for spectrum in matrix[group]:
            spectra.append('\t'.join([str(x) for x in spectrum[:points]]))

    texts = ['%s,%s,%s' % (filename, name, str(klass)) for
             filename, name, klass in zip(dfiles, names, klasses)]
    text = '\n'.join(texts)

    open(directory + os.sep + r'save2.txt', 'w').write('\n'.join(spectra))
    open(directory + os.sep + r'save3.csv', 'w').write(text)
#
#
def get_line_range(fpath, common):
    """Returns initial and final indexes of lines to be read from file <fpath>.

    Returns a tuple (line_ini, line_end).
    Stored in other places as:
        - 'line_range'   (function parameter)
        - 'Maldi.offset' (global variable).

    """
    log3.debug("glr, Maldi.offset= %s Maldi.offset_temp= %s"
               % (common.offset, common.offset_temp))

    if common.range_changed:
        log3.debug('glr, Range changed')
        common.range_changed = False
        common.offset = list(common.offset_temp)

    line_ini, line_end = common.offset
    #
    if line_ini < 1 or line_end < 2:
        txt_lines = open(fpath).readlines()
        line_ini, line_end = 1, len(txt_lines)
        common.offset = [line_ini, line_end]
        common.offset_temp = [line_ini, line_end]
        common.original = (line_ini, line_end)
    return line_ini, line_end
#
#
def made_bigmat(matrix, num, klasses):
    """ Build a MxN matrix for statistical methods.

    - matrix: list of list
    - num: dictionary {klass_1: number_of_files, }
    - klasses: list

    returns
    - matrix: rows   --> experiments, spectra
              columns -> variables, maldi points
              spectra are ordered by klass in klasses
    - points: number of spectra per klass

    """
    points = []
    big_matrix = None
    #
    # num.keys()                       # ['group4', 'group1', 'group2']
    #
    for klass in klasses:
        if klass in num:              # num[klass] = counts
            try:
                big_matrix.extend(matrix[klass])  # must specify klass because
                points.append(num[klass])         # dictionary loses ordering
            except AttributeError:
                big_matrix = matrix[klass]
                points.append(num[klass])
    #
    if big_matrix:
        return big_matrix, points
    else:
        return None, None
#
#
def build_matrix(collection=None, data=None, common=None):
    """Builds dictionary {key1:list_1, key2:list_2,...}.

    collection : {klass:[file_paths],...}
    common : is klass Common

    returns dictionary with:
        - key : klass (train4, test1...)
        - values : list of values (maldi) in each file

    The order of files in each group corresponds to the list in collections

    """
    data = data or {}
    collection = collection or {}
    full = {}
    num = {}
    ar = []
    #
    for klass, dfile_paths in collection.items():  # klass ,[full paths,...]
        counts = 0
        full[klass] = []
        # noinspection PyUnresolvedReferences
        for dfile_path in dfile_paths:
            try:
                # noinspection PyUnboundLocalVariable,PyUnresolvedReferences
                ar = load_file(dfile_path, line_range, data)
            except IOError:
                tell('IOError para %s' % dfile_path)
                continue
            except UnboundLocalError:
                line_range = get_line_range(dfile_path, common)
                ar = load_file(dfile_path, line_range, data)
            #
            # file out of range -> load_file returns None
            if not ar:
                continue
            #
            full[klass].append(ar[:])
            counts += 1
        num[klass] = counts

    # noinspection PyTypeChecker
    num_scans = len(ar)
    return full, num, num_scans
#
#
def clock_threaded(_object):
    """"""
    mark = [r'|', r'/', r'--', '\\']
    mod = scipy.mod
    i = 0
    while _object.time_control:
        time.sleep(1)
        # noinspection PyTypeChecker
        label = 'Calculating   %s' % mark[mod(i, 4)]
        _object.bt_run.SetLabel(label)
        i += 1
    else:
        _object.time_control = 1
        _object.bt_run.SetLabel(_object.label)
#
#
def norm01(my_array):
    """Scale lowest bin to 0, highest bin to +1"""
    size = my_array.shape
    a = 0
    while a < size[0]:
        diff_my_array_min = my_array[a, :] - min(my_array[a, :])
        diff_max_min = max(my_array[a, :]) - min(my_array[a, :])
        my_array[a, :] = diff_my_array_min / diff_max_min
        a += 1
    return my_array
#
#
def load_file(fpath='', line_range=(1, 1000),
              kwargs=None, check_size=True, trimming=10):
    """Load data selected by column and line from a file of separated values.

    Values in the file are disposed in columns and separated by any valid
    separator chart (comma, semicolon, ).

    line_range: tuple with start and end line to be read from file.
    kwargs: dictionary with keys 'column_x', 'column_y' and 'separator',
            parameters used to extract data from the file of separated values.
    check_size: bool. Forces file length equal to the value in line_range
    trimming: number of points from the end of the file which are not read

    """
    line_ini, line_end = line_range
    line_end -= trimming
    line_ini -= 1
    #
    try:
        lines = open(fpath).readlines()
    except IOError:
        tell('File %s could not exist' % fpath)
        return None
    #
    if check_size:
        if len(lines) < line_end:
            msg = 'File %s with less than %i points' % (fpath, line_end)
            tell(msg)
            log3.debug('lfl, %s' % msg)
            return None
    #
    if not kwargs:
        kwargs = {}
        column_y = kwargs['column_y'] = 0
        # noinspection PyUnusedLocal
        column_x = kwargs['column_x'] = None
        separator = kwargs['separator'] = '\t'
    else:
        try:
            column_y = kwargs['column_y'] - 1
            separator = kwargs['separator']
        except (KeyError, TypeError):              # cases when 1.- key=''
            column_y = kwargs['column_y'] = 0      # .          2.- kwargs=None
            separator = kwargs['separator'] = '\t'
    #
    spectrum = []
    if kwargs['column_x'] is None:
        for line in lines[line_ini:line_end]:
            try:
                valor_y = line.split(separator)[column_y].strip()
                spectrum.append(float(valor_y))
            except (ValueError, IndexError):
                log3.error('lfl, Failure reading row %s, with '
                           'separator %s and column %i'
                           % (line, repr(separator), column_y + 1))
                # some files have a last blank line producing an IndexError
                # ValueError protects from incorrect numbers (i.e 1,4 or 'a')
                # Corresponding files are not taken into account
                return None
    else:
        column_x = kwargs['column_x'] - 1
        for line in lines[line_ini:line_end]:
            try:
                valor_y = line.split(separator)[column_y].strip()
                valor_x = line.split(separator)[column_x].strip()
                spectrum.append((float(valor_x), float(valor_y)))
            except (ValueError, IndexError):
                log3.error('lfl, Failure reading row %s, with'
                           'separator %s and column %i'
                           % (line, repr(separator), column_x + 1))
                return None
    #
    log3.info('lfl, -- read -- %s' % fpath)
    #
    return spectrum
#
#
def normalize_list(a_list):
    """"""
    my_array = scipy.array([a_list])
    return list(norm01(my_array)[0])
