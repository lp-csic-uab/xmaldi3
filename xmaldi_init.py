#!/usr/bin/env python
# -*- coding: latin-1 -*-
#
"""
read_and_plot 1.05 (xmaldi 201)
10 october 2009

-->
"""
#
TITLE = ' Data Selector vs.1.05'
#
import wx
import os
import pickle
import logging
from commons.iconic import Iconic
from xmaldi_fcheck import FileCheck
from xmaldi_notebook import Maldi
from xmaldi_images import ICON
from xmaldi_common import Common
from xmaldi_init_class import MyInit
#
#
log1 = logging.getLogger(__name__)
#
# noinspection PyUnusedLocal
class InitWin(MyInit, Iconic):
    """"""
    def __init__(self):
        """"""
        MyInit.__init__(self, None)
        Iconic.__init__(self, icon=ICON)
        #
        self.init_dir = Common.data_maldi_dir       # initial reference
        self.searched_dir = None
        self.line_range = (-1, -1)
        self.data = {'column_y': 1, 'column_x': None, 'separator': '\t'}
        self.combo_selected = 0
        self.combo.SetValue('A')
        #
        self.Bind(wx.EVT_BUTTON, self.on_bt_load, self.bt_load)
        self.Bind(wx.EVT_SPINCTRL, self.on_spin, self.spin)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_new_group, self.combo)
        self.Bind(wx.EVT_COMBOBOX, self.on_list_select, self.combo)
        self.Bind(wx.EVT_BUTTON, self.on_bt_chk, self.bt_chk)
        self.Bind(wx.EVT_BUTTON, self.on_bt_transformer, self.bt_trf)
        self.Bind(wx.EVT_BUTTON, self.on_bt_ok, self.bt_ok)
        self.Bind(wx.EVT_CLOSE, self.on_close_window)
    #
    def set_data(self, line_range, data, init_dir):
        """Set several instance attributes used for file data reading.

        Called by a FileCheck instance

        - line_range: (self.line_ini, self.line_end)
        - data: dictionary with info to read rows and columns from a file:
        - data: dict(column = self.column,
                    separator = self.sep)
        - init_dir: self.init_dir

        """
        self.line_range = line_range
        self.data = data
        self.init_dir = init_dir

        log1.debug('sdt, Received 1 %s, 2 %s, 3 %s'
                   % (self.line_range, self.data, init_dir))
    #
    def init_list(self, number):
        """"""
        initial = self.combo.GetCount()
        if number < initial:
            for i in range(number, initial):
                self.combo.Delete(i)
        else:
            for i in range(initial, number):
                self.combo.Append(chr(65 + i))
    #
    def on_list_select(self, evt):
        """"""
        index = self.combo.GetSelection()
        self.combo_selected = index
    #
    def on_spin(self, evt):
        """Selects number of groups"""
        number = self.spin.GetValue()
        self.init_list(int(number))
    #
    def on_new_group(self, evt):
        """"""
        group = self.combo.GetValue()
        # noinspection PyArgumentList
        self.combo.SetString(self.combo_selected, group)
    #
    def on_bt_load(self, evt):
        """Loads an experiment structure previously saved.

        data loaded is a pickled dictionary:
            dic={group:[files],....., data:{reading data}???}

        """
        self.experiment = ''
        #
        dialog = wx.FileDialog(self, "Choose an Experiment File",
                               defaultDir=Common.data_maldi_dir,
                               wildcard="xmaldi experiment|*.xme|all|*.*")
        if dialog.ShowModal() == wx.ID_OK:
            self.experiment = dialog.GetPath()
        dialog.Destroy()
        #
        if not self.experiment:
            return
        #
        self.experiment_path = os.path.join(Common.data_maldi_dir,
                                            self.experiment)
        try:
            src_collection = pickle.load(open(self.experiment_path, 'rb'))
        except (pickle.UnpicklingError, ValueError):
            return
        # the dictionary loses the order and there is nothing in data
        # to re-sort again.
        self.groups = [item for item in src_collection['order']]
        self.data = src_collection['data']
        self.line_range = src_collection['lines']
        self.files = [src_collection[group] for group in self.groups]

        try:
            maldi = Maldi(self.groups, self.line_range,
                          self.data, self.init_dir, self.files)
        except AttributeError:
            return
        #
        maldi.Show()
    #
    def on_bt_chk(self, evt):
        """"""
        fcheck = FileCheck(self, init_dir=self.init_dir)
        fcheck.Show()
    #
    def on_bt_ok(self, evt):
        """"""
        number = self.combo.GetCount()
        self.groups = []
        for i in range(number):
            self.groups.append(self.combo.GetString(i))
        #
        print('en InitWin ', self.init_dir)
        maldi = Maldi(self.groups, self.line_range, self.data, self.init_dir)
        maldi.Show()
    #
    def on_close_window(self, evt):
        self.Destroy()
    #
    def on_bt_transformer(self, evt):
        """Opens Transformer window.

        Magic to bring module xmaldi_trf.TransFrame
        whom would be self-referenced as it import classes (pe SpectrumMaldi)
        from xmaldi_main process through which it would be recalled
        if the import were in the header
        (for any imported class, the header is imported)
        """
        from xmaldi_trf import TransFrame
        if os.path.isfile(self.init_dir):
            Common.offset = self.line_range
        transformer = TransFrame(self.line_range, self.data, self.init_dir)
        transformer.Show()


if __name__ == '__main__':
    app = wx.App()
    InitWin().Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
