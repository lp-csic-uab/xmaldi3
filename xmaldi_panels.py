#!/usr/bin/env python
# -*- coding: latin-1 -*-
#
"""
panels 11 (xmaldi 201)
10 October 2009
"""
#
#
from matplotlib import use
use('WXAgg')
import pickle
import copy
import os
import wx
import _thread as thread
import logging
import scipy as sp
from numpy.linalg.linalg import LinAlgError as LAErr
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.lines import Line2D
from matplotlib.patches import Circle
# this requires Gnuplot to be installed. Should be fixed
# noinspection PyUnresolvedReferences,PyProtectedMember
from gnuplot import Gnuplot
from commons.warns import tell
from xmaldi_common import Common, collection
from xmaldi_fselect import FileSelector
from xmaldi_spec import GraphWin, SpecPcaDfa, SpecMaldi
# noinspection PyProtectedMember
from xmaldi_pchem import PCA_NIPALS, DFA_XVAL, DFA, _index
from xmaldi_funcs import made_bigmat, build_matrix, clock_threaded
from xmaldi_funcs import norm01, load_file, get_line_range
#
#
log2 = logging.getLogger(__name__)
#
#
def print_nice(arrays, names):
    """"""
    texts = []
    for array, name in zip(arrays, names):
        new_array = copy.deepcopy(array)
        text = '\n' + name + '\n' + repr(new_array.round(decimals=2))
        texts.append(text)

    return '\n'.join(texts)
#
#
def plot_data(spectrum, window, title=''):
    """Plot a series of values.

    -spectrum: list or array of numbers.
    -window: wx canvas object to draw to.

    """
    log2.debug('pdt, Plotting data %s' % title)
    window.axes.plot(spectrum)
    window.axes.set_title(title, fontsize=8)
    window.dibuja()
#
#
class Plot_3d(object):
    """Draw with GnuPlot.

    Draw a 3D view of the data given on a numpy array.
    It always draw the first three columns of data (PC1-3, DF1-3) independently
    of which are selected on window.

    """
    def __init__(self, tdt, points, choices):
        """Plot_3d constructor.

         - tdt: total data numpy array of dimensions SAMPLES X SCORES
                SAMPLES are the total number of points to draw
                SCORES are f.e. pca scores, by default = 5 Principal Components
         - points: number of points per experiment. list (p.e [5, 7, 8])
         - choices: list of labels (pe. ['PC1', 'PC2', 'PC3'])

        """
        point_old = 0
        index = 0
        #
        xx, yy, zz = [], [], []
        for point_new in points:
            if point_new > 0:
                point_new += point_old
                xx.append(tdt[point_old:point_new, 0])
                yy.append(tdt[point_old:point_new, 1])
                zz.append(tdt[point_old:point_new, 2])
                point_old = point_new
                index += 1
        #
        self.gplt = Gnuplot()
        self.gplt.xlabel(choices[0])
        self.gplt.ylabel(choices[1])
        text = 'set zlabel "' + choices[2] + '"'
        self.gplt(text)
        for idx in range(index):
            dataset = sp.transpose(sp.array([xx[idx], yy[idx], zz[idx]]))
            try:
                if idx > 0:
                    self.gplt.replot(dataset)
                else:
                    self.gplt.splot(dataset)
            except IOError:
                tell("For 3D view\nYou must install GnuPlot")
                return
#
#
class PcaWinGroups(wx.Panel):
    """"""
    def __init__(self, parent, groups=None, choices=None):
        wx.Panel.__init__(self, parent, -1)
        self.groups = groups or []
        self.choices = choices or []
        let = (70, 15)
        self.SetBackgroundColour("yellow")
        #
        self.sizer = wx.GridSizer(cols=2, hgap=2, vgap=2)
        self.instances = {}
        #
        self.create_choices(let)
        self.SetSizer(self.sizer)
        self.Fit()
    #
    # noinspection PyArgumentList
    def create_choices(self, let):
        """"""
        choices = len(self.choices)
        self.sizer.Add(wx.Panel(self, size=let), 1)
        self.sizer.Add(wx.Panel(self, size=let), 1)
        for idx, eachLabel in enumerate(self.groups):
            self.sizer.Add(wx.StaticText(self, label=eachLabel, size=let,
                           style=wx.ALIGN_RIGHT))
            instance = wx.Choice(self, choices=self.choices)
            if idx + 1 >= choices:
                instance.SetSelection(0)
            else:
                instance.SetSelection(idx)
            self.sizer.Add(instance, 0)
            self.instances[eachLabel] = instance
#
#
class SpectrumMaldi(SpecMaldi):
    """Canvas to draw spectra"""
    def __init__(self, parent, xlabel='x_lab', ylabel='y_lab', choices=None):
        choices = choices or []
        SpecMaldi.__init__(self, parent, xlabel, ylabel, choices)
        self.parent = parent
    #
    def add_toolbar(self):
        SpecMaldi.add_toolbar(self)
        self.control.data_ini.WriteText(str(Common.offset[0]))
        self.control.data_fin.WriteText(str(Common.offset[1]))
        #
        self.toolbar.update()
    #
    def get_maldi_range_changed(self):
        return Common.range_changed
    #
    def get_offset(self):
        return Common.offset
    #
    def get_offset_original(self):
        return Common.original
    #
    def set_offset(self, line_ini=0, line_end=0):
        if line_ini is None:
            line_ini = Common.offset_temp[0]
        if line_end is None:
            line_end = Common.offset_temp[1]
        #
        # solo = MaldiOffset si hay plot
        Common.offset_temp = [line_ini, line_end]
        log2.info('sof, Assign offset_temp %s %s' % (line_ini, line_end))
        Common.range_changed = True
    #
    def on_norm(self, evt):
        """"""
        Common.normalize = True if self.cbx_norm.IsChecked() else False
#
#
class SpectrumStat(SpecPcaDfa):
    """"""
    def __init__(self, parent, xlabel='x_lab', ylabel='y_lab', choices=None):
        """Constructor"""
        choices = choices or []
        SpecPcaDfa.__init__(self, parent, xlabel, ylabel, choices)
    #
    def get_offset(self):
        return Common.offset
#
#
# noinspection PyUnusedLocal,PyAttributeOutsideInit
class PcaPanel(wx.Panel):
    """Principal Component Analysis panel in xmaldi notebook.

    Rebuild with wxGlade

    """
    def __init__(self, parent, groups=None, data=None):
        """"""
        wx.Panel.__init__(self, parent, -1)
        class_choices = ['group1', 'group2', 'group3', 'group4',
                         'group5', 'group6', 'group7', 'None']
        self.choices = ['PC1', 'PC2', 'PC3', 'PC4', 'PC5']
        self.result_dict = {}
        self.groups = groups or []
        self.data = data or {}
        self.label = 'Run PCA'
        self.running = False
        self.selected = None
        self.cid = None
        self.time_control = 1
        #
        self.pan_ti = GraphWin(self, xlabel='PC1', ylabel='PC2',
                               choices=self.choices)
        self.pan_td = PcaWinGroups(self,
                                   groups=self.groups, choices=class_choices)
        self.bt_run = wx.Button(self, label=self.label, name='PcaPanel')
        self.pan_bi = SpectrumStat(self, xlabel='x', ylabel='y')
        self.tc_report = wx.TextCtrl(self, -1, "",
                                     style=wx.TE_MULTILINE |
                                     wx.TE_READONLY | wx.TE_RICH2)
        #
        self.Bind(wx.EVT_CHOICE, self.on_choice, self.pan_ti.choice_x_box)
        self.Bind(wx.EVT_CHOICE, self.on_choice, self.pan_ti.choice_y_box)
        # noinspection PyProtectedMember
        self.Bind(wx.EVT_BUTTON, self.on_3d, self.pan_ti._3d)
        self.Bind(wx.EVT_BUTTON, self.on_run, self.bt_run)
        self.Bind(wx.EVT_BUTTON, self.on_loads, self.pan_bi.loads)
        #
        self.__set_properties()
        self.__do_layout()
        # end wxGlade
    #
    def __set_properties(self):
        # begin wxGlade: MyFrame.__set_properties
        self.SetMinSize((460, 400))
        # end wxGlade
    #
    # noinspection PyArgumentList
    def __do_layout(self):
        # begin wxGlade: MyFrame.__do_layout
        grid_sizer_1 = wx.GridSizer(2, 2, 0, 0)
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_1.Add(self.pan_ti, 1, wx.EXPAND, 0)
        sizer_1.Add(self.pan_td, 1, wx.EXPAND, 0)
        sizer_1.Add(self.bt_run, 0, wx.EXPAND, 0)
        grid_sizer_1.Add(sizer_1, 1, wx.EXPAND, 0)
        grid_sizer_1.Add(self.pan_bi, 1, wx.EXPAND, 0)
        grid_sizer_1.Add(self.tc_report, 0, wx.EXPAND, 0)
        self.SetSizer(grid_sizer_1)
        grid_sizer_1.Fit(self)
        self.Layout()
        # end wxGlade
    #
    def on_loads(self, evt):
        import copy
        value = self.pan_bi.loads_select.GetValue() - 1
        mat = copy.deepcopy(self.pca_loads)
        try:
            plot_data(sp.absolute(mat.transpose())[:, value], self.pan_bi)
        except AttributeError:
            return
    #
    def on_run(self, evt):
        if self.running:
            return
        evt.Skip()
    #
    def calcScipy(self, kollection=collection, klasses=None):
        """Calculate PCA scores using threads.

        Called from Nbook by on_run_(self, event)
        which is itself triggered by an event risen from here (PcaPanel)
        This is the PyChem version
        - groups = ['Control', 'Case', 'C']
        - klasses = ['group1', 'group4', 'test1', 'test4']
        - collection = dictionary(klass1=[file list],...)

        """
        if not klasses:
            klasses = []
        thread.start_new_thread(clock_threaded, (self,))
        thread.start_new_thread(self.calcScipy_threaded, (kollection, klasses))
    #
    def calcScipy_threaded(self, kollection, klasses):
        """Calculate PCA scores

        {klass:[full_paths,...],...}
        """
        self.running = True
        matrix, num, num_scans = build_matrix(kollection, self.data, Common)
        self.klasses = klasses
        self.collection = kollection
        bigmat, self.points = made_bigmat(matrix, num, klasses)
        #
        try:
            self.pca_scores, self.pca_loads, pr, eigens = PCA_NIPALS(bigmat, 5)
        except UnboundLocalError:
            return
        #
        text = print_nice((self.pca_scores, self.pca_loads, pr, eigens),
                          ('pca_scores', 'pca_loads', 'pca_pr', 'pca_eigens'))
        self.tc_report.WriteText(text)
        self.draw_pca_scores()
        self.time_control = 0
        self.running = False
    #
    def on_3d(self, evt):
        """"""
        try:
            self.plot = Plot_3d(self.pca_scores, self.points, self.choices[0:3])
        except AttributeError:
            return
    #
    def draw_pca_scores(self, x=0, y=1):
        """"""
        try:
            pca_scores = self.pca_scores
        except AttributeError:
            return
        self.pan_ti.axes.clear()
        self.pan_ti.xlabel = self.choices[x]
        self.pan_ti.ylabel = self.choices[y]

        colors = ['r', 'g', 'y', 'b', 'c', 'm', 'k']
        marks = ['o', 'x', '+']
        mark_colors = [color + mark for mark in marks for color in colors]
        #
        self.pan_ti.canvas.mpl_disconnect(self.cid)
        point_old = 0
        for index, point_new in enumerate(self.points):
            point_new += point_old
            mark = mark_colors[index]
            xx, yy, cc = (pca_scores[point_old:point_new, x],
                          pca_scores[point_old:point_new, y],
                          mark)
            self.pan_ti.axes.plot(xx, yy, cc, picker=5)
            point_old = point_new
        #
        self.cid = self.pan_ti.canvas.mpl_connect('pick_event', self.on_pick)
        self.pan_ti.dibuja()
    #
    def get_choice(self):
        xchoice = self.choices.index(
            self.pan_ti.choice_x_box.GetStringSelection())
        ychoice = self.choices.index(
            self.pan_ti.choice_y_box.GetStringSelection())
        return xchoice, ychoice
    #
    def on_choice(self, evt):
        xchoice, ychoice = self.get_choice()
        self.draw_pca_scores(xchoice, ychoice)
    #
    def on_pick(self, evt):
        """"""
        if isinstance(evt.artist, Line2D):
            line = evt.artist
            xdata = line.get_xdata()
            ydata = line.get_ydata()
            ind = evt.ind
            pos_data = zip(sp.take(xdata, ind), sp.take(ydata, ind))
            self.search(next(pos_data))
    #
    # noinspection PyUnboundLocalVariable
    def search(self, data):
        """"""
        xdata, ydata = data
        lines, cols = self.pca_scores.shape
        for file_index in range(lines):
            found = 0
            for val in self.pca_scores[file_index, :].tolist():
                if abs(xdata - val) < 0.0001:
                    found += 1
                if abs(ydata - val) < 0.0001:
                    found += 1
            if found > 1:
                log2.debug('src, File_index %s' % file_index)
                break
        #
        total = -1
        for index, points in enumerate(self.points):
            total += points
            if file_index > total:
                continue
            else:
                class_index = index
                file_class_index = file_index - total + points - 1
                break
        #
        klass = self.klasses[class_index]
        dfile = self.collection[klass][file_class_index]
        line_range = get_line_range(dfile, Common)
        spec = load_file(dfile, line_range, self.data)
        plot_data(spec, self.pan_bi, dfile)
        self.selected = dfile
#
#
# noinspection PyPep8Naming,PyAttributeOutsideInit
class DfaPanel(wx.Panel):
    """Discriminant Function Analysis panel in xmaldi notebook.

    Rebuild with wxGlade

    """""
    def __init__(self, parent, groups=None, data=None):
        """"""
        wx.Panel.__init__(self, parent, -1)
        class_choices = ['train1', 'train2', 'train3', 'train4',
                         'test1', 'test2', 'test3', 'test4', 'None']
        self.choices = ['DF1', 'DF2']
        self.result_dict = {}
        self.groups = groups or []
        self.data = data or []
        self.label = 'Run DFA'
        self.running = False
        self.selected = None
        self.cid = None
        self.time_control = 1
        #
        self.pan_ti = GraphWin(self, xlabel='DF1', ylabel='DF2',
                               choices=self.choices)
        self.pan_td = PcaWinGroups(self, groups=self.groups,
                                   choices=class_choices)
        self.ckb_xvalid = wx.CheckBox(self, -1, "X-validation")
        self.lb_pcs = wx.StaticText(self, -1, "# PCs")
        self.spc_pcs = wx.SpinCtrl(self, -1, "5", min=0, max=100)
        self.lb_dfs = wx.StaticText(self, -1, "# DFs")
        self.spc_dfs = wx.SpinCtrl(self, -1, "4", min=0, max=100)
        self.bt_run = wx.Button(self, -1, label=self.label, name='DfaPanel')
        self.pan_bi = SpectrumStat(self, xlabel='x', ylabel='y')
        self.tc_report = wx.TextCtrl(self, -1, "",
                                     style=wx.TE_MULTILINE | wx.TE_READONLY |
                                     wx.TE_RICH2)
        #
        self.Bind(wx.EVT_CHOICE, self.on_choice, self.pan_ti.choice_x_box)
        self.Bind(wx.EVT_CHOICE, self.on_choice, self.pan_ti.choice_y_box)
        # noinspection PyProtectedMember
        self.Bind(wx.EVT_BUTTON, self.on_3d, self.pan_ti._3d)
        self.Bind(wx.EVT_BUTTON, self.on_run, self.bt_run)
        self.Bind(wx.EVT_BUTTON, self.on_loads, self.pan_bi.loads)
        #
        self.__set_properties()
        self.__do_layout()
        # end wxGlade
    #
    def __set_properties(self):
        # begin wxGlade: MyFrame.__set_properties
        self.SetMinSize((460, 400))
        self.spc_pcs.SetMinSize((40, -1))
        self.spc_dfs.SetMinSize((40, -1))
        # end wxGlade
    #
    # noinspection PyArgumentList
    def __do_layout(self):
        # begin wxGlade: MyFrame.__do_layout
        grid_sizer_1 = wx.GridSizer(2, 2, 0, 0)
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        grid_sizer_1.Add(self.pan_ti, 1, wx.EXPAND, 0)
        sizer_1.Add(self.pan_td, 1, wx.EXPAND, 0)
        sizer_2.Add(self.ckb_xvalid, 0, wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 10)
        sizer_2.Add(self.lb_pcs, 0, wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 15)
        sizer_2.Add(self.spc_pcs, 0, wx.LEFT, 8)
        sizer_2.Add(self.lb_dfs, 0, wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 10)
        sizer_2.Add(self.spc_dfs, 0, wx.LEFT, 8)
        sizer_2.Add((20, 20), 1, wx.EXPAND, 0)
        sizer_1.Add(sizer_2, 0, wx.LEFT | wx.TOP | wx.BOTTOM | wx.EXPAND, 3)
        sizer_1.Add(self.bt_run, 0, wx.EXPAND, 0)
        grid_sizer_1.Add(sizer_1, 1, wx.EXPAND, 0)
        grid_sizer_1.Add(self.pan_bi, 1, wx.EXPAND, 0)
        grid_sizer_1.Add(self.tc_report, 1, wx.EXPAND, 0)
        self.SetSizer(grid_sizer_1)
        grid_sizer_1.Fit(self)
        self.Layout()
        # end wxGlade
    #
    # noinspection PyUnusedLocal
    def on_loads(self, evt):
        """"""
        value = self.pan_bi.loads_select.GetValue() - 1
        try:
            plot_data(sp.absolute(self.dfa_loads[:, value]), self.pan_bi)
        except AttributeError:
            return
    #
    def on_run(self, evt):
        if self.running:
            return
        evt.Skip()
    #
    def calcScipy(self, kollection=collection, klasses=None):
        """Calculates DFA scores.

        This is the PyChem version.
        - groups = ['Control', 'Case', 'C']
        - klasses = ['train1', 'train4', 'test1', 'test4']
        - collection = dictionary(klass1=[list of files],...)

        """
        klasses = klasses or []
        thread.start_new_thread(clock_threaded, (self,))
        thread.start_new_thread(self.calcScipy_threaded, (kollection, klasses))

    def calcScipy_threaded(self, kollection, klasses):
        """Calculates DFA scores.

        - klass:[full paths,...],...}
        tt, pp, pr, eigens = pca_scores, pca_loadings, PCPe
        groups = ['Control', 'Case'
        rVar, pca_eigens= PCA_NIPALS(matrix, 5)
        -Use PCA scores   ------->    tt
        -data
        - PCs    ----------------->  pca_scores (integer)
        - Discriminant Functions ->  dfa_scores (integer)
        - Cross Validation   ----->  xvalid(boolean)

        """
        log2.debug('cst, klasses: %s' % klasses)
        self.running = True
        self.klasses = klasses
        self.collection = kollection
        self.plot = None
        pcs = self.spc_pcs.GetValue()
        dfs = self.spc_dfs.GetValue()
        self.xvalid = self.ckb_xvalid.IsChecked()
        pca_type = 'covar'
        #
        self.choices = ['DF' + str(i) for i in range(1, dfs + 1)]
        self.pan_ti.choice_x_box.SetItems(self.choices)
        self.pan_ti.choice_y_box.SetItems(self.choices)
        self.pan_ti.choice_x_box.SetSelection(0)
        self.pan_ti.choice_y_box.SetSelection(1)
        #
        matrix, num, num_scans = build_matrix(kollection, self.data, Common)
        bigmat, self.points = made_bigmat(matrix, num, klasses)
        #
        klass_cols = []
        for index, number in enumerate(self.points):
            klass_code = [index + 1]
            klass_cols.extend(klass_code * number)
        #
        # Run PCA
        try:
            pca_scores, pca_loads, pr, eigens = PCA_NIPALS(bigmat, pcs)
        except UnboundLocalError:
            return
        #
        if self.xvalid:
            try:
                # Run DFA PCs full xvalid NIPALS
                self.mask = self.get_mask(self.points)
                self.dfa_scores, self.dfa_loads, dfa_eigens =\
                    DFA_XVAL(sp.array(bigmat), pcs,
                             sp.array(klass_cols)[:, sp.newaxis], '',
                             sp.array(self.mask)[:, sp.newaxis],
                             dfs, ptype=pca_type
                             )
            except LAErr:
                log2.error('cst, LinAlgError')
                tell('Error. Maybe too low number of classes')
                self.time_control = 0
                self.running = False
                return
            except TypeError:
                log2.error('cst, TypeError')
                tell('Error. Maybe too low number of classes')
                self.time_control = 0
                self.running = False
                return
        else:
            # Run DFA PCs no xvalid
            tt_dfs = pca_scores[:, 0:dfs]

            if tt_dfs.shape[1] == sp.array(bigmat).shape[1]:
                self.dfa_scores, self.dfa_loads, dfa_eigens, dummy = \
                   DFA(tt_dfs, klass_cols, dfs)
            else:
                self.dfa_scores, dummy, dfa_eigens, self.dfa_loads = \
                   DFA(tt_dfs, klass_cols, dfs, pca_loads[0:dfs, :])
        #
        self.klass_cols = klass_cols
        #
        text = print_nice((self.dfa_scores, self.dfa_loads, dfa_eigens),
                          ('dfa_scores', 'dfa_loads', 'dfa_eigens'))
        self.tc_report.WriteText(text)
        self.draw_dfa_scores()
        #
        self.time_control = 0
        self.running = False
    #
    def get_mask(self, points):
        """Produces a mask for 50% of the samples randomly"""
        mask_cols = []
        for number in points:
            if number < 4:
                mask = [0] * number
                mask_cols.extend(mask)
                continue
            mask = list(sp.random.randint(0, 2, number))
            dif = mask.count(0) - mask.count(1)
            count = 0
            if dif > 3:
                left = int(dif / 2)
                for index in range(len(mask)):
                    # noinspection PySimplifyBooleanCheck
                    if mask[index] == 0:
                        mask[index] = 1
                        count += 1
                        if count == left:
                            break
            if - dif > 1:
                left = int(- dif / 2)
                for index in range(len(mask)):
                    if mask[index] == 1:
                        mask[index] = 0
                        count += 1
                        if count == left:
                            break
            num_tests = mask.count(1)
            tests = list(sp.random.randint(1, 3, num_tests))
            dif = tests.count(1) - tests.count(2)
            count = 0
            if dif < 0:
                left = int(-dif / 2)
                for index in range(len(tests)):
                    if tests[index] == 2:
                        tests[index] = 1
                        count += 1
                        if count == left:
                            break
            count = 0
            for index in range(len(mask)):
                if mask[index] == 1:
                    mask[index] = tests[count]
                    count += 1

            mask_cols.extend(mask)
        return mask_cols
    #
    def draw_dfa_scores(self, x=0, y=1):
        """Draw dfa scores and error circles"""
        try:
            error_circles = self.get_error_circles(self.dfa_scores,
                                                   self.klass_cols, x, y)
        except AttributeError:
            return
        dfa_scores = self.dfa_scores
        self.pan_ti.axes.clear()
        self.pan_ti.xlabel = self.choices[x]
        self.pan_ti.ylabel = self.choices[y]
        point_old = 0
        colors = ['r', 'g', 'y', 'b', 'c', 'm', 'k']
        marks = ['o', 'x', '+', '^', '>']
        mark_colors = [color + mark for mark in marks for color in colors]
        #
        self.pan_ti.canvas.mpl_disconnect(self.cid)
        #
        if self.xvalid:
            index = 0
            for color_idx, point_new in enumerate(self.points):
                point_new += point_old
                for point in range(point_old, point_new):
                    xx = (dfa_scores[point, x],)
                    yy = (dfa_scores[point, y],)
                    cc = colors[color_idx % 7] + marks[self.mask[index]]
                    self.pan_ti.axes.plot(xx, yy, cc, picker=5)
                    index += 1
                point_old = point_new
        else:
            for color_idx, point_new in enumerate(self.points):
                point_new += point_old
                mark = mark_colors[color_idx % 5]

                xx, yy, cc = (dfa_scores[point_old:point_new, x],
                              dfa_scores[point_old:point_new, y],
                              mark)
                self.pan_ti.axes.plot(xx, yy, cc, picker=5)
                point_old = point_new
        #
        for color_idx, circle in enumerate(error_circles):
            color = colors[color_idx % 7]
            patch = Circle(circle, radius=2.15, fill=False, edgecolor=color)
            self.pan_ti.axes.add_patch(patch)
        #
        self.cid = self.pan_ti.canvas.mpl_connect('pick_event', self.on_pick)
        self.pan_ti.dibuja()
    #
    def get_error_circles(self, dfa_scores, cl, col1=0, col2=1):
        """Calculates mean centres for a dfa/cva plot scaled to unit variance.

        - 95% confidence radius is 2.15

        sp.take(a, indices, axis=None, out=None, mode='raise')
        _index(y,num) "use this to get tuple index for take"

        """
        scores = dfa_scores[:, (col1, col2)]
        #
        if (scores.shape[1] > 1) & (col1 != col2) is True:
            nScores, nCl = sp.zeros((1, 2)), []
            for i in range(1, int(max(cl)) + 1):
                index = _index(sp.array(cl)[:, sp.newaxis], i)
                media = sp.mean(sp.take(scores, index, 0), 0)
                nScores = sp.concatenate((nScores, media[sp.newaxis]))
                nCl.append(i)
        else:
            return []

        centres = nScores[1:int(max(cl)) + 1, 0:2]
        #
        return centres
    #
    def on_pick(self, evt):
        """"""
        if isinstance(evt.artist, Line2D):
            line = evt.artist
            xdata = line.get_xdata()
            ydata = line.get_ydata()
            ind = evt.ind
            pos_data = list(zip(sp.take(xdata, ind), sp.take(ydata, ind)))
            self.search(pos_data)
    #
    def get_choice(self):
        """ """
        xchoice = self.choices.index(
            self.pan_ti.choice_x_box.GetStringSelection())
        ychoice = self.choices.index(
            self.pan_ti.choice_y_box.GetStringSelection())

        return xchoice, ychoice
    #
    # noinspection PyUnusedLocal
    def on_choice(self, evt):
        xchoice, ychoice = self.get_choice()
        self.draw_dfa_scores(xchoice, ychoice)
    #
    # noinspection PyUnusedLocal
    def on_3d(self, evt):
        try:
            self.plot = Plot_3d(self.dfa_scores, self.points, self.choices[0:3])
        except AttributeError:
            return
    #
    # noinspection PyUnboundLocalVariable
    def search(self, data):
        """"""
        lin, col = self.dfa_scores.shape
        for file_index in range(lin):
            found = 0
            for val in self.dfa_scores[file_index, :].tolist():
                if abs(data[0][0] - val) < 0.0001:
                    found += 1
                if abs(data[0][1] - val) < 0.0001:
                    found += 1
            if found > 1:
                log2.debug('src, file_index %s' % file_index)
                break
        #
        total = -1
        index = -1
        for points in self.points:
            index += 1
            total += points
            if file_index > total:
                continue
            else:
                class_index = index
                file_class_index = file_index - total + points - 1
                break
        #
        print(self.collection)
        klass = self.klasses[class_index]
        file_ = self.collection[klass][file_class_index]
        lines = get_line_range(file_, Common)
        spec = load_file(file_, lines, self.data)
        plot_data(spec, self.pan_bi, file_)
        #
        self.selected = file_
#
#
class FileToolBand(wx.Panel):
    def __init__(self, parent):
        # begin wxGlade: MyFrame.__init__
        wx.Panel.__init__(self, parent)
        self.parent = parent
        self.SetSize((619, 97))
        self.panel_1 = wx.Panel(self, wx.ID_ANY)
        self.bt_a_avg = wx.Button(self.panel_1, wx.ID_ANY, "AVG",
                                  name='|___on_a_avg')
        self.bt_a_mix = wx.Button(self.panel_1, wx.ID_ANY, "HEAT MIX",
                                  name='|___on_a_mix')
        self.bt_a_sel_all = wx.Button(self.panel_1, wx.ID_ANY, "SEL ALL",
                                      name='|___on_a_sel_all')
        self.bt_a_del_all = wx.Button(self.panel_1, wx.ID_ANY, "DEL ALL",
                                      name='|___on_a_del_all')
        self.bt_a_show = wx.Button(self.panel_1, wx.ID_ANY, "SHOW",
                                   name='|___on_a_show')
        self.bt_save = wx.Button(self.panel_1, wx.ID_ANY, "SAVE EXPERIMENT",
                                 name='|___on_save')

        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.StaticBoxSizer(
            wx.StaticBox(self.panel_1, wx.ID_ANY,
                         "APPLY TO ALL FILES IN ALL GROUPS"),
            wx.HORIZONTAL)
        sizer_2.Add(self.bt_a_avg, 0, 0, 0)
        sizer_2.Add(self.bt_a_mix, 0, 0, 0)
        sizer_2.Add(self.bt_a_sel_all, 0, 0, 0)
        sizer_2.Add(self.bt_a_del_all, 0, 0, 0)
        sizer_2.Add(self.bt_a_show, 0, 0, 0)
        sizer_2.Add((20, 20), 0, 0, 0)
        sizer_2.Add(self.bt_save, 0, 0, 0)
        self.panel_1.SetSizer(sizer_2)
        sizer_1.Add(self.panel_1, 0, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        sizer_1.SetSizeHints(self)
        self.Layout()
        # end wxGlade

        self.Bind(wx.EVT_BUTTON, self.on_a_avg, self.bt_a_avg)
        self.Bind(wx.EVT_BUTTON, self.on_a_mix, self.bt_a_mix)
        self.Bind(wx.EVT_BUTTON, self.on_a_sel_all, self.bt_a_sel_all)
        self.Bind(wx.EVT_BUTTON, self.on_a_del_all, self.bt_a_del_all)
        self.Bind(wx.EVT_BUTTON, self.on_a_show, self.bt_a_show)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.bt_save)

    def on_a_avg(self, evt):
        """
        make average of all files, all groups
        """
        evt.Skip()
    #
    def on_a_mix(self, evt):
        """
        experiment_path: pickled file with dictionary:
           {group:[files],....., data:{reading data}???}
        """
        evt.Skip()
    #
    def on_a_sel_all(self, evt):
        """
        experiment_path: pickled file with dictionary:
           {group:[files],....., data:{reading data}???}
        """
        evt.Skip()
    #
    def on_a_del_all(self, evt):
        """
        experiment_path: pickled file with dictionary:
           {group:[files],....., data:{reading data}???}
        """
        evt.Skip()
    #
    def on_a_show(self, evt):
        """
        experiment_path: pickled file with dictionary:
           {group:[files],....., data:{reading data}???}
        """
        evt.Skip()
    #
    def on_save(self, evt):
        """
        experiment_path: pickled file with dictionary:
           {group:[files],....., data:{reading data}???}
        """
        evt.Skip()
#
#
class FileToolColumn(FileSelector):
    """File selector tab in xmaldi notebook."""
    def __init__(self, parent, name='hello'):
        FileSelector.__init__(self, parent, name)
        self.parent = parent
    #
    def get_buttons(self):
        """Modify for different buttons"""
        return dict(Load=('LOAD', 'on_load'),
                    Avrg=('AVG', 'on_avg'),
                    Mix=('MIX', 'on_mix'),
                    SelAll=('SEL ALL', 'on_sel_all'),
                    Del=('DELETE', 'on_delete'),
                    DelAll=('DEL ALL', 'on_del_all'),
                    Show=('SHOW', 'on_show')
                    )
    #
    def get_init_dir(self):
        return Common.init_dir
    #
    def set_init_dir(self, directory):
        Common.init_dir = directory
#
#
# noinspection PyUnusedLocal,PyAttributeOutsideInit
class HeatPanel(wx.Panel):
    """Heat map panel in xmaldi notebook."""
    def __init__(self, parent, groups=None):
        if not groups:
            groups = []
        wx.Panel.__init__(self, parent)
        self.groups = groups
        self.subplots = []
        self.xmax, self.xmin = 0, 0        # max number of points
        self.yranges = []
        self.yrange = (0, 0)               # original ymin y ymax
        self.factor_min, self.factor_max = 100, 100   # factor for height adjust
        self.pox1, self.pox2 = 0, 0        # used to mark xlim(x1,x2)
        self.memorize = False
        self.figure = Figure(figsize=(3.5, 5))
        self.canvas = FigureCanvas(self, -1, self.figure)
        #
        self.add_tools()
    #
    # noinspection PyArgumentList
    def add_tools(self):
        """"""
        buttons = dict(Refresh=('REFRESH', 'on_refresh'),
                       MoveR=('-->', 'on_right'),
                       MoveL=('<--', 'on_left'),
                       Back=('BACK', 'on_back'),
                       Save=('SAVE', 'on_save'),
                       Clear=('CLEAR', 'on_clear'),
                       )

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        self.instances = {}
        for key, prop in buttons.items():
            instance = wx.Button(self, -1, prop[0], size=(55, 20))
            self.Bind(wx.EVT_BUTTON, getattr(self, prop[1]), instance)
            sizer2.Add(instance)
            self.instances[key] = instance
        #
        self.spinner_min = wx.SpinCtrl(self, size=(50, 20),
                                       min=1, max=500, initial=100)
        self.spinner_max = wx.SpinCtrl(self, size=(50, 20),
                                       min=1, max=500, initial=100)
        self.bt_range = wx.Button(self, label='min set_range max',
                                  size=(115, 20))
        self.Bind(wx.EVT_BUTTON, self.on_new_range, self.bt_range)

        sizer2.Add(wx.Panel(self, size=(50, 10)), 0, flag=wx.EXPAND)
        sizer2.Add(self.spinner_min, 0, flag=wx.EXPAND)
        sizer2.Add(self.bt_range, 0, flag=wx.EXPAND)
        sizer2.Add(self.spinner_max, 0, flag=wx.EXPAND)
        #
        sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        sizer.Add(sizer2)
        self.SetSizer(sizer)
        self.Fit()
    #
    def add_plot(self, tab, num_plots, index):
        """ """
        self.xmax = len(tab[0])
        #
        ymin = tab.min()
        ymax = tab.max()
        #
        try:
            self.yranges[index - 1] = (ymin, ymax)
        except IndexError:
            self.yranges = [(None, None)] * num_plots
            self.yranges[index - 1] = (ymin, ymax)
        #
        if not self.memorize:
            self.yrange = (ymin, ymax)
        log2.debug("apl, Self range %s" % str(self.yrange))
        #
        self.axes = self.figure.add_subplot(num_plots, 1, index)
        self.subplots.append(self.axes)
        self.figure.canvas.mpl_connect('button_press_event', self.on_press)
        self.figure.canvas.mpl_connect('button_release_event', self.on_release)
        #
        self.axes.imshow(tab, aspect='auto', vmin=self.yrange[0],
                         vmax=self.yrange[1], interpolation='nearest')
        self.axes.set_title(self.groups[index - 1])
        self.axes.set_axisbelow(True)
        if not index == num_plots:
            self.axes.set_xticks([])
        #
        # dont know what is this for....
        if self.memorize:
            for subplot in self.subplots:
                subplot.set_xlim(self.pox1, self.pox2)
        else:
            self.pox2 = self.xmax
        #
        self.canvas.draw()
    #
    def on_refresh(self, evt):
        """"""
        for subplot in self.subplots:
            subplot.set_xlim(0, self.xmax)
        self.pox1 = 0
        self.pox2 = self.xmax
        self.canvas_draw()
    #
    def on_right(self, evt):
        """"""
        xnew = min(self.xmax, self.pox2 + 50)
        offset = xnew - self.pox2
        self.pox2 = xnew
        self.pox1 = self.pox1 + offset
        for item in self.subplots:
            item.set_xlim(self.pox1, self.pox2)
        self.canvas_draw()

    def on_left(self, evt):
        """"""
        xnew = max(0, self.pox1 - 50)
        offset = self.pox1 - xnew
        self.pox1 = xnew
        self.pox2 = self.pox2 - offset
        for item in self.subplots:
            item.set_xlim(self.pox1, self.pox2)
        self.canvas_draw()

    def on_back(self, evt):
        tell('on_back')

    def on_save(self, evt):
        dlg = wx.FileDialog(self, "Save to file",
                            "", "", "png |*.png | bmp|*.bmp",
                            wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT |
                            wx.FD_CHANGE_DIR)
        if dlg.ShowModal() == wx.ID_OK:
            self.canvas.print_figure(dlg.GetPath())

    def on_clear(self, evt):
        """"""
        self.figure.clear()
        self.canvas_draw()

    def on_press(self, evt):
        self.xop = evt.xdata  # xop= 'x on press' to not interfere

    def on_release(self, evt):
        if self.xop and evt.xdata:
            self.pox2 = evt.xdata
        else:
            return
        #
        pox1 = min(self.xop, self.pox2)
        pox2 = max(self.xop, self.pox2)
        #
        if (pox2 - pox1) < 10:
            self.pox1 = 0
        else:
            for item in self.subplots:
                item.set_xlim(pox1, pox2)
            self.xop = 0
            self.pox1 = pox1
            self.pox2 = pox2
            self.canvas_draw()

    def canvas_draw(self):
        """"" """""
        try:
            self.figure.canvas.draw()
        except KeyError:
            log2.error("cdw, Canvas draw KeyError")
            return

    def on_new_range(self, evt):
        """new intensity range limits"""
        self.memorize = True
        self.factor_min = 0.01 * self.spinner_min.GetValue()
        self.factor_max = 0.01 * self.spinner_max.GetValue()
        #
        try:
            minimum = min([x[0] for x in self.yranges])
            maximum = max([x[1] for x in self.yranges])
        except ValueError:
            log2.error("onr, on_new_range ValueError")
            return
        #
        try:
            ymax = self.factor_max * maximum
            dist = maximum - minimum
            ymin = min(ymax, minimum + (dist * (self.factor_min - 1)))
            self.yrange = ymin, ymax
        except TypeError:           # minimum o maximum=None
            log2.error("onr, on_new_range TypeError")
            return
        evt.Skip()
#
#
class MaldiPanel(wx.Panel):
    """First Nbook panel, with file data and maldi visor.

    --> groups = ['A','B','C','D']
    --> self.A = FileToolColumn(self.sel_panel, name='A', width=len(groups))

    """
    init_dir = ''
    #
    # noinspection PyArgumentList,PyUnresolvedReferences
    def __init__(self, parent, groups=None, data=None, files=None):
        if not files:
            files = []
        wx.Panel.__init__(self, parent)
        self.parent = parent
        self.data = data or {}
        self.groups = groups or []
        #
        self.clicked_mass = 0
        self.figure = None
        self.subplot = []
        self.pox1 = 0
        self.pox2 = 0
        self.lock = thread.allocate_lock()
        #
        sizer1 = wx.BoxSizer(wx.HORIZONTAL)
        sizer2 = wx.BoxSizer(wx.VERTICAL)
        #
        self.maldi = SpectrumMaldi(self)
        self.ftband = FileToolBand(self)
        #
        files_panel = wx.Panel(self)
        #
        for group in self.groups:
            group_instance = FileToolColumn(files_panel, name=group)
            setattr(self, group, group_instance)
        #
        self.Bind(wx.EVT_LISTBOX_DCLICK, self.on_list)
        self.Bind(wx.EVT_BUTTON, self.on_signal)
        #
        for group in self.groups:
            sizer1.Add(getattr(self, group), 1, wx.EXPAND)
        #
        files_panel.SetSizer(sizer1)
        files_panel.Fit()
        #
        sizer2.Add(wx.Panel(self), 0, wx.EXPAND)
        sizer2.Add(files_panel, 1, wx.EXPAND)
        sizer2.Add(self.ftband, 0, wx.EXPAND)
        sizer2.Add(wx.Panel(self), 0, wx.EXPAND)
        sizer2.Add(self.maldi, 1, wx.EXPAND)
        #
        self.SetSizer(sizer2)
        self.Fit()
        #
        if not(len(files) == 0 or len(files[0]) == 0):
            for index, group in enumerate(self.groups):
                getattr(self, group).checklist.Clear()
                getattr(self, group).current_dir = files[index][0]
                for arch in files[index][1:]:
                    getattr(self, group).checklist.Append(arch)

    # noinspection PyUnusedLocal,PyUnresolvedReferences
    def on_save(self, evt):
        """Saves filenames and data setup from an analysis for easy reload.

        It serializes (pickle) filepath, read parameters and groups structure
        in order to be reloaded from the start page of the application without
        needing to setup them again.

        Data is saved with the extension '.xme' (xmaldi experiment)

        """
        #
        data_maldi_dir = Common.data_maldi_dir
        dialog = wx.FileDialog(self, "Save a Experiment File", data_maldi_dir)
        if dialog.ShowModal() == wx.ID_OK:
            experiment_file = dialog.GetPath()
        else:
            return
        dialog.Destroy()
        dictionary = {'data': self.data,
                      'lines': (Common.offset[0], Common.offset[1])}

        log2.info('osv, Self.groups in save= %s' % self.groups)
        order = []
        for group in self.groups:
            order.append(group)
            dfiles = []
            win = getattr(self, group)
            checked = win.checklist.GetCount()
            if not checked:
                continue
            # noinspection PyUnresolvedReferences
            dfiles.append(win.current_dir)
            for i in range(checked):
                if win.checklist.IsChecked(i):
                    dfile = win.checklist.GetString(i)
                    dfiles.append(dfile)
            dictionary[group] = dfiles
        dictionary['order'] = order
        dictionary['init_dir'] = Common.init_dir
        save_path = os.path.join(data_maldi_dir, '%s.xme' % experiment_file)
        pickle.dump(dictionary, open(save_path, 'wb'), 2)
    #
    # noinspection PyUnresolvedReferences
    def on_list(self, evt):
        evobj = evt.GetEventObject()
        group = getattr(self, evobj.GetName())
        dfile = group.checklist.GetStringSelection()
        dfile_path = group.current_dir + os.sep + dfile
        if Common.range_changed:
            self.maldi.clean()
        lines = get_line_range(dfile_path, Common)
        spectrum = load_file(dfile_path, lines, self.data)
        if spectrum:
            self.maldi.xlim = self.maldi.axes.viewLim.intervalx
            self.plot_spectrum(spectrum, label=dfile, clean=False)
    #
    def on_signal(self, evt):
        """Retrieve events from objects on child panels and reroute them
        to the corresponding call in this frame
        """

        co = evt.GetEventObject()
        # wid[0] = '|'/'control'/'case'/...
        # wid[1] = on_mix/onMerge/...etc
        wid = co.GetName().split('___', 1)
        if wid[0] == '|':
            func = getattr(self, wid[1])
            func(None)
        else:
            try:
                func = getattr(self, wid[1])
                # noinspection PyCallingNonCallable
                func(wid)
            except AttributeError:
                evt.Skip()
    #
    # noinspection PyUnusedLocal
    def on_a_avg(self, evt):
        for group in self.groups:
            win = getattr(self, group)
            checked = win.checklist.GetCount()
            if not checked:
                continue
            self.on_avg((group,))
    #
    # noinspection PyUnusedLocal
    def on_a_sel_all(self, evt):
        for group in self.groups:
            win = getattr(self, group)
            n = win.checklist.GetCount()
            if not n:
                return

            mark = 0
            for i in range(n):
                if not win.checklist.IsChecked(i):
                    mark = 1
                    break
            for item in range(n):
                # noinspection PyArgumentList
                win.checklist.Check(item, mark)
            win.files = win.get_files_selected()
    #
    # noinspection PyUnusedLocal
    def on_a_del_all(self, evt):
        for group in self.groups:
            win = getattr(self, group)
            count = win.checklist.GetCount()
            for index in range(count):
                win.checklist.Delete(count - index - 1)
    #
    # noinspection PyUnusedLocal
    def on_a_show(self, evt):
        for group in self.groups:
            self.on_show((group,))
    #
    def on_avg(self, wid):
        thread.start_new_thread(self.threaded_on_avg, (wid,))

    # noinspection PyUnresolvedReferences,PyAugmentAssignment
    # noinspection PyUnboundLocalVariable
    def threaded_on_avg(self, wid):
        """"""
        parent = getattr(self, wid[0])
        if not parent.files:
            return
        self.lock.acquire_lock()
        nmr = 0
        out = 0
        if Common.range_changed:
            self.maldi.clean()
        line_range = get_line_range(parent.files[0], Common)
        for arch in parent.files:
            # do not normalize
            spectrum = load_file(arch, line_range, self.data)
            #
            if not spectrum:
                out += 1
                continue
            #
            nmr += 1
            np_spectrum = sp.array(spectrum)
            try:
                avg = avg + np_spectrum
            except NameError:
                avg = np_spectrum
        #
        try:
            avg = avg / nmr
        except UnboundLocalError:       # p.e. "referenced before assignment"
            aviso("""Major error reading file\n
                     Try selecting a smaller "ini-end" range
                  """)
            self.lock.release_lock()
            return

        if Common.normalize:
            avg = avg[:, sp.newaxis]
            avg = avg.swapaxes(0, 1)
            avg = norm01(avg)
            avg = avg[0]
        #
        out_text = '(%i out)' % out if out else ''
        #
        self.plot_spectrum(avg, label='Averaged %i %s files from %s'
                                      % (nmr, out_text, wid[0]))
        self.lock.release_lock()

    def on_show(self, wid):
        thread.start_new_thread(self.threaded_on_show, (wid,))

    # noinspection PyUnresolvedReferences
    def threaded_on_show(self, wid):
        """Plot spectra from files selected on listbox <wid>.

        #TODO: In order not to overwrite spectra it should be converted to
        sp.array and made spec+offset. Offset should be calculated taking into
        account the intensity of the ions and/or with a selector.
        A button in maldiPanel to select whether the offset is desired or not
        would be required

        """
        parent = getattr(self, wid[0])
        if not parent.files:
            return
        self.lock.acquire_lock()
        if Common.range_changed:
            self.maldi.clean()
        lines = get_line_range(parent.files[0], Common)
        #
        for arch in parent.files:
            spec_name = os.path.basename(arch)
            spec = load_file(arch, lines, self.data)
            if spec:
                self.plot_spectrum(spec, label=spec_name, clean=False)
        self.lock.release_lock()

    def on_mix(self, wid):
        wid.Skip()

    def plot_spectrum(self, intensity,
                      label='no_file', clean=True):
        """Plot spectrum and labels given in label.

        - clean : if True, return axes to full size.
        Should be accelerated for many files
        plot_spectrum method here is identical to method in TransFrame...

        """
        mass_axis = range(len(intensity))
        lines = self.maldi.axes.get_lines()
        color = 'b'
        #
        if len(lines) < 2:
            clean = True         # ??
        #
        if not clean:
            self.maldi.xlim = self.maldi.axes.viewLim.intervalx
            self.maldi.ylim = self.maldi.axes.viewLim.intervaly
        #
        self.maldi.axes.plot(mass_axis, intensity)
        lines = self.maldi.axes.get_lines()
        self.maldi.control.data_ini.Clear()
        self.maldi.control.data_fin.Clear()
        self.maldi.control.data_ini.WriteText(str(Common.offset[0]))
        self.maldi.control.data_fin.WriteText(str(Common.offset[1]))
        #
        try:
            color = lines[-1].get_color()
        except IndexError:
            log2.error('1139 plot file IndexError')
        #
        max_lines = 10
        if len(lines) < max_lines:
            ypos = 0.9 - (0.06 * len(lines))
            name = '--- ' + label
        else:
            ypos = 0.9 - (0.06 * max_lines)
            name = '--- more than' + str(max_lines)
        self.maldi.figure.text(0.6, ypos, name, fontsize=7, color=color)
        self.maldi.dibuja(clean)


if __name__ == '__main__':

    class MyFrame(wx.Frame):
        def __init__(self):
            wx.Frame.__init__(self, None, size=(500, 450))

    mydata = {'separator': '\t', 'column_y': 1, 'column_x': None}
    path = os.path.join(os.getcwd(), "test/1/1B.txt")
    app = wx.App()

    frame_1 = MyFrame()
    frame_2 = MyFrame()
    frame_3 = MyFrame()
    frame_4 = MyFrame()
    MaldiPanel(frame_1, groups=['A', 'B'], data=mydata)
    PcaPanel(frame_2, groups=['A', 'B'], data=mydata)
    DfaPanel(frame_3, groups=['A', 'B'], data=mydata)
    HeatPanel(frame_4)
    for frame in [frame_1]:
        frame.Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
