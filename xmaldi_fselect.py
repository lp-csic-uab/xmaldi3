#!/usr/bin/env python
# -*- coding: latin-1 -*-
#
"""
file_selector 1.05 (xmaldi 201)
10 october 2009

-->
"""
#
TITLE = ' Data Selector vs.1.05'
#
import wx
import os
import logging
#
log6 = logging.getLogger(__name__)
#
#
# noinspection PyUnusedLocal
class FileSelector(wx.Panel):
    """Base class for XMaldiFileSelector and TransformerFileSelector"""
    # noinspection PyArgumentList
    def __init__(self, parent, name='hello'):
        wx.Panel.__init__(self, parent, -1, size=(300, 100))
        self.files = []                  # .
        self.instances = {}              # dict of buttons
        #
        self.text = wx.StaticText(self, -1,
                                  label='%15s' % name, pos=(0, 0), size=(0, 20))
        self.checklist = wx.CheckListBox(self, name=name,
                                         style=wx.LB_HSCROLL)

        sizer_1 = wx.BoxSizer(wx.VERTICAL)            # main
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer_3 = wx.BoxSizer(wx.VERTICAL)       # bottom-right
        sizer_1.Add(self.text, 0, wx.EXPAND, 0)
        sizer_2.Add(self.checklist, 1, wx.LEFT | wx.RIGHT | wx.EXPAND, 5)
        sizer_2.Add(self.sizer_3, 1, wx.EXPAND, 0)
        sizer_1.Add(sizer_2, 1, wx.EXPAND, 5)

        self.build_buttons(name)

        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        self.Layout()
    #
    def get_buttons(self):
        """Modify for different buttons"""
        return dict(Dummy1=('DUMMY', 'on_dummy'),
                    Dummy2=('DUMMY', 'on_dummy'),
                    )
    #
    def build_buttons(self, name=''):
        """Creates selection buttons."""
        buttons = self.get_buttons()

        for key, prop in buttons.items():
            # to sent the instance to whom it belongs
            # and the Button name
            fullname = name + '___' + prop[1]
            instance = wx.Button(self, -1,
                                 prop[0], size=(50, 20), name=fullname)

            self.Bind(wx.EVT_BUTTON, getattr(self, prop[1]), instance)
            self.sizer_3.Add(instance)
            self.instances[key] = instance
    #
    def on_dummy(self, evt):
        pass
    #
    def on_load(self, evt):
        """"""
        current_dir = ''
        #
        # noinspection PyUnresolvedReferences
        self.current_dir = self.get_init_dir()
        log6.debug('old, Current dir: %s' % self.current_dir)
        #
        if os.path.isfile(self.current_dir):
            self.current_dir = os.path.dirname(self.current_dir)
        #
        dialog = wx.DirDialog(self, "Choose a dir", self.current_dir)
        if dialog.ShowModal() == wx.ID_OK:
            current_dir = dialog.GetPath()
        dialog.Destroy()
        #
        if not current_dir:
            return
        self.current_dir = current_dir
        self.checklist.Clear()
        for archivo in os.listdir(self.current_dir):
            if os.path.isfile(os.path.join(self.current_dir, archivo)):
                self.checklist.Append(archivo)
        #
        # noinspection PyUnresolvedReferences
        self.set_init_dir(self.current_dir)
    #
    def on_save(self, evt):
        """
        experiment_path: pickled file with dictionary:
           {group:[files],....., data:{reading data}???}
        """
        evt.Skip()
    #
    def on_delete(self, evt):
        """Delete checked files from file selector list."""
        index = self.checklist.GetSelection()
        if index < 0:
            return
        key = self.checklist.GetStringSelection()
        self.checklist.Delete(index)
    #
    def on_del_all(self, evt):
        count = self.checklist.GetCount()
        for index in range(count):
            self.checklist.Delete(count - index - 1)
    #
    def on_sel_all(self, evt):
        """Selects and deselects (marks(unmarks) all files in a group.

        """
        n = self.checklist.GetCount()
        if not n:
            return

        mark = 0
        for i in range(n):
            if not self.checklist.IsChecked(i):
                mark = 1
                break
        for item in range(n):
            # noinspection PyArgumentList
            self.checklist.Check(item, mark)
        self.files = self.get_files_selected()
    #
    def on_avg(self, evt):
        self.files = self.get_files_selected()
        evt.Skip()
    #
    def on_mix(self, evt):
        self.files = self.get_files_selected()
        evt.Skip()
    #
    def on_show(self, evt):
        self.files = self.get_files_selected()
        evt.Skip()
    #
    def get_files_selected(self):
        n = self.checklist.GetCount()
        if not n:
            return
        j = 0
        dfile_paths = []
        for i in range(n):
            if self.checklist.IsChecked(i):
                fpath = self.current_dir + os.sep + self.checklist.GetString(i)
                dfile_paths.append(fpath)
        return dfile_paths


if __name__ == '__main__':
    app = wx.App()
    f = wx.Frame(None)
    fs = FileSelector(f)
    f.Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
