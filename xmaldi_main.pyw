#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
xmaldi_main
Main App. Opens Init Frame
11 october 2008 - 5 jan 2013
"""
#
import os
import wx
import configparser
# noinspection PyUnresolvedReferences
import logging
import logging.config
from xmaldi_common import Common
#
logging.config.fileConfig(Common.log_file)
log = logging.getLogger(__name__)
#
from xmaldi_init import InitWin
#
#
def read_config():
    """Initializes parameters in global class Commons"""
    config = configparser.ConfigParser()
    config.read_file(open('config.ini'))
    #
    for section in config.sections():
        for key, value in config.items(section):
            setattr(Common, key, value)
            log.debug('%s : %s' % (key, value))
#
def create_directories():
    """Create xmaldi data folders """
    for directory_tree in (Common.data_maldi_dir,):
        try:
            os.makedirs(directory_tree)
        except OSError:
            log.warning('%s failed or already exist' % directory_tree)


if __name__ == '__main__':
    app = wx.App()
    #
    read_config()
    create_directories()
    #
    InitWin().Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
